-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-10-2017 a las 22:28:01
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pagoimpuesto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atributo`
--

CREATE TABLE `atributo` (
  `OIDAtributo` varchar(32) NOT NULL,
  `codigoAtributo` int(11) NOT NULL,
  `fechaHabilitacionAtributo` date NOT NULL,
  `fechaInhabilitacionAtributo` date DEFAULT NULL,
  `nombreAtributo` varchar(45) NOT NULL,
  `OIDTipoDato` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `atributo`
--

INSERT INTO `atributo` (`OIDAtributo`, `codigoAtributo`, `fechaHabilitacionAtributo`, `fechaInhabilitacionAtributo`, `nombreAtributo`, `OIDTipoDato`) VALUES
('1', 1, '2017-09-18', NULL, 'Fecha de vencimiento', '3'),
('2', 2, '2017-09-18', NULL, 'Importe', '4'),
('3', 3, '2017-09-18', NULL, 'Fecha proximo vencimiento', '3'),
('4', 4, '2017-09-18', NULL, 'Monto minimo exigible', '4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE `banco` (
  `OIDBanco` varchar(32) NOT NULL,
  `fechaHabilitacionBanco` date NOT NULL,
  `fechaInhabilitacionBanco` date DEFAULT NULL,
  `cuitBanco` varchar(255) DEFAULT NULL,
  `nombreBanco` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banco`
--

INSERT INTO `banco` (`OIDBanco`, `fechaHabilitacionBanco`, `fechaInhabilitacionBanco`, `cuitBanco`, `nombreBanco`) VALUES
('1', '2017-09-18', NULL, '142536', 'Banco Nacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `OIDCliente` varchar(32) NOT NULL,
  `apellidoCliente` varchar(255) NOT NULL,
  `dniCliente` int(11) NOT NULL,
  `nombreCliente` varchar(255) NOT NULL,
  `numClienteBanco` int(11) NOT NULL,
  `fechaHabilitacionCliente` date NOT NULL,
  `fechaInhabilitacionCliente` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`OIDCliente`, `apellidoCliente`, `dniCliente`, `nombreCliente`, `numClienteBanco`, `fechaHabilitacionCliente`, `fechaInhabilitacionCliente`) VALUES
('1', 'Garay', 36731121, 'Fernando', 1, '2017-09-18', NULL),
('2', 'Guerrero', 36731345, 'Maxi', 2, '2017-09-18', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `OIDCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionCuenta` date NOT NULL,
  `fechaInhabilitacionCuenta` date DEFAULT NULL,
  `numeroCuenta` int(11) DEFAULT NULL,
  `OIDTipoCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`OIDCuenta`, `fechaHabilitacionCuenta`, `fechaInhabilitacionCuenta`, `numeroCuenta`, `OIDTipoCuenta`, `OIDCliente`, `OIDBanco`) VALUES
('1', '2017-09-18', NULL, 1, '1', '1', '1'),
('2', '2017-09-18', NULL, 2, '2', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `OIDEmpresa` varchar(255) NOT NULL,
  `cuitEmpresa` varchar(255) NOT NULL,
  `fechaHabilitacionEmpresa` date NOT NULL,
  `fechaInhabilitacionEmpresa` date DEFAULT NULL,
  `nombreEmpresa` varchar(50) NOT NULL,
  `OIDTipoEmpresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`OIDEmpresa`, `cuitEmpresa`, `fechaHabilitacionEmpresa`, `fechaInhabilitacionEmpresa`, `nombreEmpresa`, `OIDTipoEmpresa`) VALUES
('1', '1', '2017-09-18', NULL, 'Claro', '1'),
('2', '2', '2017-09-18', NULL, 'Movistar', '1'),
('3', '23132', '2017-09-11', NULL, 'ECOGas', '1'),
('402881e95eb00f1e015eb00f25170000', '11223344', '2017-09-23', NULL, 'Claro', NULL),
('402881e95eb00f1e015eb00f251d0001', '24524524527', '2017-09-23', NULL, 'Edemsa', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresatipoimpuesto`
--

CREATE TABLE `empresatipoimpuesto` (
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL,
  `codigoempresatipoimpuesto` int(11) NOT NULL,
  `fechaInhabilitacionEmpresaTipoImpuesto` date DEFAULT NULL,
  `fechaHabilitacionEmpresaTipoImpuesto` date NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresatipoimpuesto`
--

INSERT INTO `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`, `codigoempresatipoimpuesto`, `fechaInhabilitacionEmpresaTipoImpuesto`, `fechaHabilitacionEmpresaTipoImpuesto`, `OIDTipoImpuesto`, `OIDEmpresa`) VALUES
('1', 1, NULL, '2017-09-18', '1', '1'),
('2', 2, NULL, '2017-09-18', '1', '2'),
('3', 3, NULL, '2017-09-11', '3', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresatipoimpuestoatributo`
--

CREATE TABLE `empresatipoimpuestoatributo` (
  `OIDEmpresaTipoImpuestoAtributo` varchar(255) NOT NULL,
  `orden` varchar(255) NOT NULL,
  `formato` varchar(255) DEFAULT NULL,
  `valorPeriodicidad` varchar(255) DEFAULT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresatipoimpuestoatributo`
--

INSERT INTO `empresatipoimpuestoatributo` (`OIDEmpresaTipoImpuestoAtributo`, `orden`, `formato`, `valorPeriodicidad`, `OIDTipoImpuestoAtributo`, `OIDEmpresaTipoImpuesto`) VALUES
('1', '0', NULL, NULL, '1', '1'),
('2', '1', NULL, NULL, '2', '1'),
('3', '2', NULL, NULL, '3', '1'),
('4', '3', NULL, NULL, '4', '1'),
('5', '0', NULL, NULL, '1', '2'),
('6', '1', NULL, NULL, '2', '2'),
('7', '2', NULL, NULL, '4', '2'),
('8', '0', NULL, NULL, '5', '3'),
('9', '1', NULL, NULL, '6', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacion`
--

CREATE TABLE `operacion` (
  `OIDOperacion` varchar(255) NOT NULL,
  `codpagoelect` varchar(255) NOT NULL,
  `fechavencimientocomprobanteimpago` date NOT NULL,
  `fechaoperacion` date NOT NULL,
  `importeOperacion` double NOT NULL,
  `numeroComprobante` int(11) NOT NULL,
  `numerooperacion` int(11) NOT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDEmpresaTipoImpuesto` varchar(255) DEFAULT NULL,
  `OIDCuenta` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operacion`
--

INSERT INTO `operacion` (`OIDOperacion`, `codpagoelect`, `fechavencimientocomprobanteimpago`, `fechaoperacion`, `importeOperacion`, `numeroComprobante`, `numerooperacion`, `OIDTipoImpuesto`, `OIDEmpresaTipoImpuesto`, `OIDCuenta`, `OIDCliente`) VALUES
('402881e95ed5e7be015ed5e80a8c0000', '123', '2017-10-01', '2017-10-01', 250, 2, 1, '1', '1', '1', '1'),
('402881e95ed5e7be015ed5e871b30003', '123', '2017-10-01', '2017-10-01', 420, 4, 2, '1', '2', '1', '1'),
('402881e95ed5e7be015ed5e8bfa20005', '123', '2017-10-01', '2017-10-01', 500, 1, 3, '3', '3', '1', '1'),
('402881e95ed951af015ed95203460000', '123', '2017-10-01', '2017-10-01', 420, 4, 4, '1', '2', '1', '1'),
('402881e95ed956db015ed9570b550000', '123', '2017-10-01', '2017-10-01', 500, 1, 5, '3', '3', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacionatributo`
--

CREATE TABLE `operacionatributo` (
  `OIDOperacionAtributo` varchar(32) NOT NULL,
  `valorOperacionAtibuto` varchar(255) NOT NULL,
  `OIDTipoImpuestoAtributo` varchar(32) DEFAULT NULL,
  `OIDOperacion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operacionatributo`
--

INSERT INTO `operacionatributo` (`OIDOperacionAtributo`, `valorOperacionAtibuto`, `OIDTipoImpuestoAtributo`, `OIDOperacion`) VALUES
('402881e95ed5e7be015ed5e80a930001', '29-11-2017', '3', '402881e95ed5e7be015ed5e80a8c0000'),
('402881e95ed5e7be015ed5e80a930002', '75', '4', '402881e95ed5e7be015ed5e80a8c0000'),
('402881e95ed5e7be015ed5e871b30004', '100', '4', '402881e95ed5e7be015ed5e871b30003'),
('402881e95ed951af015ed952034e0001', '100', '4', '402881e95ed951af015ed95203460000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametroconexion`
--

CREATE TABLE `parametroconexion` (
  `OIDParametroConexion` varchar(255) NOT NULL,
  `codigoConexion` varchar(255) NOT NULL,
  `codigoParametroConexion` int(11) NOT NULL,
  `fechaInhabilitacionParametroConexion` date DEFAULT NULL,
  `fechaHabilitacionParametroConexion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parametroconexion`
--

INSERT INTO `parametroconexion` (`OIDParametroConexion`, `codigoConexion`, `codigoParametroConexion`, `fechaInhabilitacionParametroConexion`, `fechaHabilitacionParametroConexion`) VALUES
('1', '1', 1, NULL, '2017-09-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `OIDRol` varchar(32) NOT NULL,
  `fechaHabilitacionRol` date NOT NULL,
  `fechaInhabilitacionRol` date DEFAULT NULL,
  `codigoRol` int(11) DEFAULT NULL,
  `nombreRol` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`OIDRol`, `fechaHabilitacionRol`, `fechaInhabilitacionRol`, `codigoRol`, `nombreRol`) VALUES
('1', '2017-09-18', NULL, 1, 'ClienteBanco'),
('2', '2017-09-10', NULL, 2, 'AdministradorBanco');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolopcion`
--

CREATE TABLE `rolopcion` (
  `OIDRolOpcion` varchar(32) NOT NULL,
  `fechaHabilitacionRolOpcion` date NOT NULL,
  `fechaInhabilitacionRolOpcion` date DEFAULT NULL,
  `codigoRolOpcion` int(11) DEFAULT NULL,
  `nombreRolOpcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rolopcion`
--

INSERT INTO `rolopcion` (`OIDRolOpcion`, `fechaHabilitacionRolOpcion`, `fechaInhabilitacionRolOpcion`, `codigoRolOpcion`, `nombreRolOpcion`) VALUES
('1', '2017-09-18', NULL, 1, 'CUPAGARIMPUESTO'),
('2', '2017-09-19', NULL, 2, 'CUCONSULTAROPERACIONES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolrolopcion`
--

CREATE TABLE `rolrolopcion` (
  `OIDRol` varchar(32) NOT NULL,
  `OIDRolOpcion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rolrolopcion`
--

INSERT INTO `rolrolopcion` (`OIDRol`, `OIDRolOpcion`) VALUES
('1', '1'),
('1', '2'),
('2', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sistemabanco`
--

CREATE TABLE `sistemabanco` (
  `OIDSistemaBanco` varchar(255) NOT NULL,
  `codigoSistemaBanco` int(255) NOT NULL,
  `fechaHabilitacionSistemaBanco` date NOT NULL,
  `fechaInhabilitacionSistemaBanco` date DEFAULT NULL,
  `OIDBanco` varchar(32) DEFAULT NULL,
  `OIDParametroConexion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sistemabanco`
--

INSERT INTO `sistemabanco` (`OIDSistemaBanco`, `codigoSistemaBanco`, `fechaHabilitacionSistemaBanco`, `fechaInhabilitacionSistemaBanco`, `OIDBanco`, `OIDParametroConexion`) VALUES
('1', 1, '2017-09-18', NULL, '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocuenta`
--

CREATE TABLE `tipocuenta` (
  `OIDTipoCuenta` varchar(32) NOT NULL,
  `fechaHabilitacionTipoCuenta` date NOT NULL,
  `fechaInhabilitacionTipoCuenta` date DEFAULT NULL,
  `codigoTipoCuenta` int(11) DEFAULT NULL,
  `nombreTipoCuenta` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipocuenta`
--

INSERT INTO `tipocuenta` (`OIDTipoCuenta`, `fechaHabilitacionTipoCuenta`, `fechaInhabilitacionTipoCuenta`, `codigoTipoCuenta`, `nombreTipoCuenta`) VALUES
('1', '2017-09-18', NULL, 1, 'Caja de Ahorro'),
('2', '2017-09-18', NULL, 2, 'Cuenta Corriente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodato`
--

CREATE TABLE `tipodato` (
  `OIDTipoDato` varchar(32) NOT NULL,
  `codTipoDato` int(11) NOT NULL,
  `nombreTipoDato` varchar(255) NOT NULL,
  `fechaHabilitacionTipoDato` date NOT NULL,
  `fechaInhabilitacionTipoDato` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipodato`
--

INSERT INTO `tipodato` (`OIDTipoDato`, `codTipoDato`, `nombreTipoDato`, `fechaHabilitacionTipoDato`, `fechaInhabilitacionTipoDato`) VALUES
('1', 1, 'String', '2017-09-18', NULL),
('2', 2, 'int', '2017-09-18', NULL),
('3', 3, 'Date', '2017-09-18', NULL),
('4', 4, 'double', '2017-09-18', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoempresa`
--

CREATE TABLE `tipoempresa` (
  `OIDTipoEmpresa` varchar(255) NOT NULL,
  `codigoTipoEmpresa` int(11) NOT NULL,
  `nombreTipoEmpresa` varchar(255) NOT NULL,
  `fechaHabTipoEmpresa` date DEFAULT NULL,
  `fechaInhabTipoEmpresa` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoempresa`
--

INSERT INTO `tipoempresa` (`OIDTipoEmpresa`, `codigoTipoEmpresa`, `nombreTipoEmpresa`, `fechaHabTipoEmpresa`, `fechaInhabTipoEmpresa`) VALUES
('1', 1, 'Servicio', '2017-09-18', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimpuesto`
--

CREATE TABLE `tipoimpuesto` (
  `OIDTipoImpuesto` varchar(255) NOT NULL,
  `codigoTipoImpuesto` int(11) NOT NULL,
  `fechaHabilitacionTipoImpuesto` date NOT NULL,
  `fechaInhabilitacionTipoImpuesto` date DEFAULT NULL,
  `modificableTipoImpuesto` bit(1) NOT NULL,
  `nombreTipoImpuesto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoimpuesto`
--

INSERT INTO `tipoimpuesto` (`OIDTipoImpuesto`, `codigoTipoImpuesto`, `fechaHabilitacionTipoImpuesto`, `fechaInhabilitacionTipoImpuesto`, `modificableTipoImpuesto`, `nombreTipoImpuesto`) VALUES
('1', 1, '2017-09-18', NULL, b'1', 'Telefonia'),
('2', 2, '2017-09-18', NULL, b'1', 'Electricidad'),
('3', 3, '2017-09-18', NULL, b'0', 'Gas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimpuestoatributo`
--

CREATE TABLE `tipoimpuestoatributo` (
  `OIDTipoImpuestoAtributo` varchar(32) NOT NULL,
  `codigoTipoImpuestoAtributo` int(11) NOT NULL,
  `OIDAtributo` varchar(32) DEFAULT NULL,
  `OIDTipoImpuesto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoimpuestoatributo`
--

INSERT INTO `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`, `codigoTipoImpuestoAtributo`, `OIDAtributo`, `OIDTipoImpuesto`) VALUES
('1', 1, '1', '1'),
('2', 2, '2', '1'),
('3', 3, '3', '1'),
('4', 4, '4', '1'),
('5', 5, '1', '3'),
('6', 6, '2', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `OIDUsuario` varchar(255) NOT NULL,
  `codigoUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(50) NOT NULL,
  `contraseñaUsuario` varchar(50) NOT NULL,
  `fechaHabilitacionUsuario` date NOT NULL,
  `fechaInhabilitacionUsuario` date DEFAULT NULL,
  `OIDRol` varchar(32) DEFAULT NULL,
  `OIDCliente` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`OIDUsuario`, `codigoUsuario`, `nombreUsuario`, `contraseñaUsuario`, `fechaHabilitacionUsuario`, `fechaInhabilitacionUsuario`, `OIDRol`, `OIDCliente`) VALUES
('1', 1, 'Fernando', '123456', '2017-09-18', NULL, '1', '1'),
('2', 2, 'Maxi123', '123456', '2017-09-11', NULL, '1', '2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `atributo`
--
ALTER TABLE `atributo`
  ADD PRIMARY KEY (`OIDAtributo`),
  ADD UNIQUE KEY `UK_d0yib8j696bhxicp6187olcbn` (`codigoAtributo`),
  ADD KEY `FK_bbgko1mw0ucuoga8wiovkxj75` (`OIDTipoDato`);

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`OIDBanco`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`OIDCliente`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`OIDCuenta`),
  ADD KEY `FK_nsw6u6h0f2y7yrcxq99venu5y` (`OIDTipoCuenta`),
  ADD KEY `FK_3qxiemyc9uaws4aw8mbe9iy9c` (`OIDCliente`),
  ADD KEY `FK_psr41x6qd7t1m3i7jy4p6qx2s` (`OIDBanco`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`OIDEmpresa`),
  ADD UNIQUE KEY `UK_eaihovwjj0b6ipcwuau45fm98` (`cuitEmpresa`),
  ADD KEY `FK_j2x2dwvhwnlxrakeev5f41x69` (`OIDTipoEmpresa`);

--
-- Indices de la tabla `empresatipoimpuesto`
--
ALTER TABLE `empresatipoimpuesto`
  ADD PRIMARY KEY (`OIDEmpresaTipoImpuesto`),
  ADD UNIQUE KEY `UK_qr2t4on5qvvo0kqky7hj14ohd` (`codigoempresatipoimpuesto`),
  ADD KEY `FK_3vsmpg3qu09pxcr59r7jwq7wt` (`OIDTipoImpuesto`),
  ADD KEY `FK_9rfnwxqnfdt39xws6vuurhq37` (`OIDEmpresa`);

--
-- Indices de la tabla `empresatipoimpuestoatributo`
--
ALTER TABLE `empresatipoimpuestoatributo`
  ADD PRIMARY KEY (`OIDEmpresaTipoImpuestoAtributo`),
  ADD KEY `FK_2q0tsyx31dogcnrgrff9opqua` (`OIDTipoImpuestoAtributo`),
  ADD KEY `FK_m3qv64ld6mksnetr1ob1dtm2b` (`OIDEmpresaTipoImpuesto`);

--
-- Indices de la tabla `operacion`
--
ALTER TABLE `operacion`
  ADD PRIMARY KEY (`OIDOperacion`),
  ADD KEY `FK_snguttj6rd8i3e75n1dl3bvx8` (`OIDTipoImpuesto`),
  ADD KEY `FK_53os52ht01w28nbseqfk4qtpe` (`OIDEmpresaTipoImpuesto`),
  ADD KEY `FK_njd548o9jlyb9ebfk9nsug2o2` (`OIDCuenta`),
  ADD KEY `FK_9qxaeeke83rxu6yh5t454508f` (`OIDCliente`);

--
-- Indices de la tabla `operacionatributo`
--
ALTER TABLE `operacionatributo`
  ADD PRIMARY KEY (`OIDOperacionAtributo`),
  ADD KEY `FK_qkhrhh46a9h00sain16mthy3u` (`OIDTipoImpuestoAtributo`),
  ADD KEY `FK_of60sp4x3udcwwilb6kgbfyic` (`OIDOperacion`);

--
-- Indices de la tabla `parametroconexion`
--
ALTER TABLE `parametroconexion`
  ADD PRIMARY KEY (`OIDParametroConexion`),
  ADD UNIQUE KEY `UK_fscgv0hrq02v1vrgciva20sno` (`codigoConexion`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`OIDRol`);

--
-- Indices de la tabla `rolopcion`
--
ALTER TABLE `rolopcion`
  ADD PRIMARY KEY (`OIDRolOpcion`);

--
-- Indices de la tabla `rolrolopcion`
--
ALTER TABLE `rolrolopcion`
  ADD KEY `FK_dhrswql6a0tbxcvgx7ffayu2d` (`OIDRolOpcion`),
  ADD KEY `FK_fp5nbbrj0ov6gs9or3cv5h2u8` (`OIDRol`);

--
-- Indices de la tabla `sistemabanco`
--
ALTER TABLE `sistemabanco`
  ADD PRIMARY KEY (`OIDSistemaBanco`),
  ADD UNIQUE KEY `UK_a75sm38e6fbpld17reh0a0ib4` (`codigoSistemaBanco`),
  ADD KEY `FK_8c6cfku4ufh9frbnbqliyk2n1` (`OIDBanco`),
  ADD KEY `FK_la4sx5kqaiisq0nvh6bvv4uwj` (`OIDParametroConexion`);

--
-- Indices de la tabla `tipocuenta`
--
ALTER TABLE `tipocuenta`
  ADD PRIMARY KEY (`OIDTipoCuenta`);

--
-- Indices de la tabla `tipodato`
--
ALTER TABLE `tipodato`
  ADD PRIMARY KEY (`OIDTipoDato`),
  ADD UNIQUE KEY `UK_prnreg6bfnobkot7veunh7v6b` (`codTipoDato`);

--
-- Indices de la tabla `tipoempresa`
--
ALTER TABLE `tipoempresa`
  ADD PRIMARY KEY (`OIDTipoEmpresa`),
  ADD UNIQUE KEY `UK_8jttnniahw40yobjfej8hth62` (`codigoTipoEmpresa`);

--
-- Indices de la tabla `tipoimpuesto`
--
ALTER TABLE `tipoimpuesto`
  ADD PRIMARY KEY (`OIDTipoImpuesto`),
  ADD UNIQUE KEY `UK_ixsqsw5v7ck9hkj80j8a73r72` (`codigoTipoImpuesto`);

--
-- Indices de la tabla `tipoimpuestoatributo`
--
ALTER TABLE `tipoimpuestoatributo`
  ADD PRIMARY KEY (`OIDTipoImpuestoAtributo`),
  ADD KEY `FK_r302upy3nphgw4moutf12igk7` (`OIDAtributo`),
  ADD KEY `FK_jh2wx2g90ti4lxwqa7gndbwfr` (`OIDTipoImpuesto`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`OIDUsuario`),
  ADD UNIQUE KEY `UK_7sigp9q9rn14h113enwi0qhfg` (`codigoUsuario`),
  ADD UNIQUE KEY `UK_7mqtl03sff379x3519wvppefi` (`nombreUsuario`),
  ADD KEY `FK_5avoy72x3xn8a1bhmmbj74aq2` (`OIDRol`),
  ADD KEY `FK_2x6dsb0hwjh8oksk5khfvdk6u` (`OIDCliente`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `atributo`
--
ALTER TABLE `atributo`
  ADD CONSTRAINT `FK_bbgko1mw0ucuoga8wiovkxj75` FOREIGN KEY (`OIDTipoDato`) REFERENCES `tipodato` (`OIDTipoDato`);

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `FK_3qxiemyc9uaws4aw8mbe9iy9c` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  ADD CONSTRAINT `FK_nsw6u6h0f2y7yrcxq99venu5y` FOREIGN KEY (`OIDTipoCuenta`) REFERENCES `tipocuenta` (`OIDTipoCuenta`),
  ADD CONSTRAINT `FK_psr41x6qd7t1m3i7jy4p6qx2s` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`);

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `FK_j2x2dwvhwnlxrakeev5f41x69` FOREIGN KEY (`OIDTipoEmpresa`) REFERENCES `tipoempresa` (`OIDTipoEmpresa`);

--
-- Filtros para la tabla `empresatipoimpuesto`
--
ALTER TABLE `empresatipoimpuesto`
  ADD CONSTRAINT `FK_3vsmpg3qu09pxcr59r7jwq7wt` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  ADD CONSTRAINT `FK_9rfnwxqnfdt39xws6vuurhq37` FOREIGN KEY (`OIDEmpresa`) REFERENCES `empresa` (`OIDEmpresa`);

--
-- Filtros para la tabla `empresatipoimpuestoatributo`
--
ALTER TABLE `empresatipoimpuestoatributo`
  ADD CONSTRAINT `FK_2q0tsyx31dogcnrgrff9opqua` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`),
  ADD CONSTRAINT `FK_m3qv64ld6mksnetr1ob1dtm2b` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`);

--
-- Filtros para la tabla `operacion`
--
ALTER TABLE `operacion`
  ADD CONSTRAINT `FK_53os52ht01w28nbseqfk4qtpe` FOREIGN KEY (`OIDEmpresaTipoImpuesto`) REFERENCES `empresatipoimpuesto` (`OIDEmpresaTipoImpuesto`),
  ADD CONSTRAINT `FK_9qxaeeke83rxu6yh5t454508f` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  ADD CONSTRAINT `FK_njd548o9jlyb9ebfk9nsug2o2` FOREIGN KEY (`OIDCuenta`) REFERENCES `cuenta` (`OIDCuenta`),
  ADD CONSTRAINT `FK_snguttj6rd8i3e75n1dl3bvx8` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`);

--
-- Filtros para la tabla `operacionatributo`
--
ALTER TABLE `operacionatributo`
  ADD CONSTRAINT `FK_of60sp4x3udcwwilb6kgbfyic` FOREIGN KEY (`OIDOperacion`) REFERENCES `operacion` (`OIDOperacion`),
  ADD CONSTRAINT `FK_qkhrhh46a9h00sain16mthy3u` FOREIGN KEY (`OIDTipoImpuestoAtributo`) REFERENCES `tipoimpuestoatributo` (`OIDTipoImpuestoAtributo`);

--
-- Filtros para la tabla `rolrolopcion`
--
ALTER TABLE `rolrolopcion`
  ADD CONSTRAINT `FK_dhrswql6a0tbxcvgx7ffayu2d` FOREIGN KEY (`OIDRolOpcion`) REFERENCES `rolopcion` (`OIDRolOpcion`),
  ADD CONSTRAINT `FK_fp5nbbrj0ov6gs9or3cv5h2u8` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`);

--
-- Filtros para la tabla `sistemabanco`
--
ALTER TABLE `sistemabanco`
  ADD CONSTRAINT `FK_8c6cfku4ufh9frbnbqliyk2n1` FOREIGN KEY (`OIDBanco`) REFERENCES `banco` (`OIDBanco`),
  ADD CONSTRAINT `FK_la4sx5kqaiisq0nvh6bvv4uwj` FOREIGN KEY (`OIDParametroConexion`) REFERENCES `parametroconexion` (`OIDParametroConexion`);

--
-- Filtros para la tabla `tipoimpuestoatributo`
--
ALTER TABLE `tipoimpuestoatributo`
  ADD CONSTRAINT `FK_jh2wx2g90ti4lxwqa7gndbwfr` FOREIGN KEY (`OIDTipoImpuesto`) REFERENCES `tipoimpuesto` (`OIDTipoImpuesto`),
  ADD CONSTRAINT `FK_r302upy3nphgw4moutf12igk7` FOREIGN KEY (`OIDAtributo`) REFERENCES `atributo` (`OIDAtributo`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FK_2x6dsb0hwjh8oksk5khfvdk6u` FOREIGN KEY (`OIDCliente`) REFERENCES `cliente` (`OIDCliente`),
  ADD CONSTRAINT `FK_5avoy72x3xn8a1bhmmbj74aq2` FOREIGN KEY (`OIDRol`) REFERENCES `rol` (`OIDRol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
