/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControlConsultarOperaciones;

import DTO.DTOConsultarOperacion;
import Entidades.Usuario;
import Persistencia.FachadaInterna;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class DecoradorConsultarOperaciones extends ExpertoConsultarOperaciones {
    
    public void iniciarTransaccion() {
        FachadaInterna.getInstancia().iniciarTransaccion();
    }
    
    public boolean verificarUsuario(String nombreUsuario){
        
        return super.verificarUsuario(nombreUsuario);
        
    }
    
    public List<DTOConsultarOperacion> consultarOperacion(String nombreUsuario,String fechaDesde, String fechaHasta) throws ParseException{
        
        return super.consultarOperacion(nombreUsuario, fechaDesde, fechaHasta);
    }
    
    
    
    
    
    public void finalizarTransaccion(){
        FachadaInterna.getInstancia().finalizarTransaccion();
        
    }
    
    
    
}
