/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControlConsultarOperaciones;

import DTO.DTOConsultarOperacion;
import DTO.DTOCriterio;
import DTO.DTOListOperacionAtributo;
import DTO.DTOTipoImpuesto;
import Entidades.Cliente;
import Entidades.Operacion;
import Entidades.OperacionAtributo;
import Entidades.Rol;
import Entidades.RolOpcion;
import Entidades.TipoImpuesto;
import Entidades.Usuario;
import Persistencia.FachadaPersistencia;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Maxi
 */
public class ExpertoConsultarOperaciones {
    boolean usuarioPermitido;
    public boolean verificarUsuario(String nombreUsuario){
        
        DTOCriterio c1 = new DTOCriterio();
        c1.setAtributo("nombreUsuario");
        c1.setOperador("=");
        c1.setValor(nombreUsuario);
        List<DTOCriterio> l1 = new ArrayList<>();
        l1.add(c1);
        Usuario usuario = (Usuario) FachadaPersistencia.getInstancia().buscar("Usuario",l1).get(0);
        
        
       
        List<DTOTipoImpuesto> lista_dto_ti = new ArrayList<>();

        Rol rol = usuario.getRol();
        List<RolOpcion> lista_rol_opcion = rol.getListRolOpcion();

        for (int i = 0; i < lista_rol_opcion.size(); i++) {
            if (lista_rol_opcion.get(i).getNombreRolOpcion().equals("CUCONSULTAROPERACIONES")) {
                
                usuarioPermitido = true;

                }

            }
        
       
        
        return usuarioPermitido;
            
        }
    
    
    public List<DTOConsultarOperacion> consultarOperacion(String nombreUsuario,String fechaDesde, String fechaHasta) throws ParseException{
        List<DTOConsultarOperacion> list_dto_consultar_operacion = new ArrayList<>();
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");

        
        
        DTOCriterio c0 = new DTOCriterio();
        c0.setAtributo("nombreUsuario");
        c0.setOperador("=");
        c0.setValor(nombreUsuario);
        List<DTOCriterio> l0 = new ArrayList<>();
        l0.add(c0);
        Usuario usuario = (Usuario) FachadaPersistencia.getInstancia().buscar("Usuario",l0).get(0);
        
        if("AdministradorBanco".equals(usuario.getRol().getNombreRol())){
        DTOCriterio c1 = new DTOCriterio();
        c1.setAtributo("fechaOperacion");
        c1.setOperador(">=");
        c1.setValor(formatoDeFecha.parse(fechaDesde));
        
        DTOCriterio c2 = new DTOCriterio();
        c2.setAtributo("fechaOperacion");
        c2.setOperador("<=");
        c2.setValor(formatoDeFecha.parse(fechaHasta));
        
        List<DTOCriterio> l1 = new ArrayList<>();
        l1.add(c1);
        l1.add(c2);
        
        List<Object> list_operacion = FachadaPersistencia.getInstancia().buscar("Operacion", l1);
        
            if(list_operacion.size() > 0){
                
                for(int i=0; list_operacion.size()>0;i++){
                    
                Operacion operacion = (Operacion) list_operacion.get(i);
                DTOConsultarOperacion dto_consultar_operacion = new DTOConsultarOperacion();
                dto_consultar_operacion.setApellidoCliente(operacion.getCliente().getapellidoCliente());
                dto_consultar_operacion.setCuitEmpresa(operacion.getEmpresaTipoImpuesto().getEmpresa().getCuitEmpresa());
                dto_consultar_operacion.setDniCliente(operacion.getCliente().getdniCliente());
                dto_consultar_operacion.setFechaOperacion(operacion.getFechaOperacion());
                dto_consultar_operacion.setFechaVencimiento(operacion.getFechaVencimientoComprobanteImpago());
                dto_consultar_operacion.setImporteOperacion(operacion.getImporteOperacion());
                dto_consultar_operacion.setNombreEmpresa(operacion.getEmpresaTipoImpuesto().getEmpresa().getNombreEmpresa());
                dto_consultar_operacion.setNombreTipoImpuesto(operacion.getTipoImpuesto().getNombreTipoImpuesto());
                dto_consultar_operacion.setNombrecliente(operacion.getCliente().getnombreCliente());
                dto_consultar_operacion.setNumeroComprobante(operacion.getNumeroComprobante());
                dto_consultar_operacion.setNumeroCuenta(operacion.getCuenta().getNumeroCuenta());
                dto_consultar_operacion.setNumeroOperacion(operacion.getNumeroOperacion());
                
                List<OperacionAtributo> list_operacion_atributo = operacion.getListOperacionAtributo();
                List<DTOListOperacionAtributo> list_dto_operacion_atributo = new ArrayList<>();
                
                    for(int j=0;list_operacion_atributo.size()>0;i++){
                        OperacionAtributo operacion_atributo = (OperacionAtributo) list_operacion_atributo.get(j);
                        DTOListOperacionAtributo dto_list_operacion_atributo = new DTOListOperacionAtributo();
                        dto_list_operacion_atributo.setNombreAtributo(operacion_atributo.getTipoImpuestoAtributo().getAtributo().getNombreAtributo());
                        dto_list_operacion_atributo.setValorOperacionAtributo(operacion_atributo.getValorOperacionAtibuto());
                        list_dto_operacion_atributo.add(dto_list_operacion_atributo);
                        
                    }
                    
                 dto_consultar_operacion.setList_operacion_atributo(list_dto_operacion_atributo);
                 list_dto_consultar_operacion.add(dto_consultar_operacion);
            
                }
            return list_dto_consultar_operacion;
            
            }else{
            return null;
            
            }
            
        }else{
        if("ClienteBanco".equals(usuario.getRol().getNombreRol())){
        Cliente cliente =usuario.getCliente();
        DTOCriterio c1 = new DTOCriterio();
        c1.setAtributo("fechaOperacion");
        c1.setOperador(">=");
        c1.setValor(formatoDeFecha.parse(fechaDesde));
        
        DTOCriterio c2 = new DTOCriterio();
        c2.setAtributo("fechaOperacion");
        c2.setOperador("<=");
        c2.setValor(formatoDeFecha.parse(fechaHasta));
        
        DTOCriterio c3 = new DTOCriterio();
        c3.setAtributo("cliente");
        c3.setOperador("=");
        c3.setValor(cliente);
        
        List<DTOCriterio> l1 = new ArrayList<>();
        l1.add(c1);
        l1.add(c2);
        l1.add(c3);
        List<Object> list_operacion = FachadaPersistencia.getInstancia().buscar("Operacion", l1);    
            
        for(int i=0;i< list_operacion.size();i++){
                    
                Operacion operacion = (Operacion) list_operacion.get(i);
                DTOConsultarOperacion dto_consultar_operacion = new DTOConsultarOperacion();
                dto_consultar_operacion.setApellidoCliente(operacion.getCliente().getapellidoCliente());
                dto_consultar_operacion.setCuitEmpresa(operacion.getEmpresaTipoImpuesto().getEmpresa().getCuitEmpresa());
                dto_consultar_operacion.setDniCliente(operacion.getCliente().getdniCliente());
                dto_consultar_operacion.setFechaOperacion(operacion.getFechaOperacion());
                dto_consultar_operacion.setFechaVencimiento(operacion.getFechaVencimientoComprobanteImpago());
                dto_consultar_operacion.setImporteOperacion(operacion.getImporteOperacion());
                dto_consultar_operacion.setNombreEmpresa(operacion.getEmpresaTipoImpuesto().getEmpresa().getNombreEmpresa());
                dto_consultar_operacion.setNombreTipoImpuesto(operacion.getTipoImpuesto().getNombreTipoImpuesto());
                dto_consultar_operacion.setNombrecliente(operacion.getCliente().getnombreCliente());
                dto_consultar_operacion.setNumeroComprobante(operacion.getNumeroComprobante());
                dto_consultar_operacion.setNumeroCuenta(operacion.getCuenta().getNumeroCuenta());
                dto_consultar_operacion.setNumeroOperacion(operacion.getNumeroOperacion());
                
                List<OperacionAtributo> list_operacion_atributo = operacion.getListOperacionAtributo();
                List<DTOListOperacionAtributo> list_dto_operacion_atributo = new ArrayList<>();
                
                    for(int j=0;j < list_operacion_atributo.size();j++){
                        OperacionAtributo operacion_atributo = list_operacion_atributo.get(j);
                        DTOListOperacionAtributo dto_list_operacion_atributo = new DTOListOperacionAtributo();
                        dto_list_operacion_atributo.setNombreAtributo(operacion_atributo.getTipoImpuestoAtributo().getAtributo().getNombreAtributo());
                        dto_list_operacion_atributo.setValorOperacionAtributo(operacion_atributo.getValorOperacionAtibuto());
                        list_dto_operacion_atributo.add(dto_list_operacion_atributo);
                        
                    }
                    
                 dto_consultar_operacion.setList_operacion_atributo(list_dto_operacion_atributo);
                 list_dto_consultar_operacion.add(dto_consultar_operacion);
            
                }
            return list_dto_consultar_operacion;
            
            }else{
            
                JOptionPane.showMessageDialog(null, "No existe operaciones en ese rango de fechas ingresada, por favor ingrese otro rango de fechas", "No existe operaciones", 0);
                return null;
            
            }    
            
            
            
        
            
            
        }
        
        
    }
    
    
    
    
    
    
}

      
            
            
            
        