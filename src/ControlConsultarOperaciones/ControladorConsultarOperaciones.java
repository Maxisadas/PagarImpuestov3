/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControlConsultarOperaciones;

import Fabricas.FabricaExpertos;
import DTO.DTOConsultarOperacion;
import Entidades.Usuario;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class ControladorConsultarOperaciones {
    private DecoradorConsultarOperaciones decorador_ConsultarOperaciones;
    
    
    public ControladorConsultarOperaciones(){
    
    FabricaExpertos fabrica_expertos = FabricaExpertos.getInstancia();
        decorador_ConsultarOperaciones = (DecoradorConsultarOperaciones) fabrica_expertos.crearExperto("Consultar Operaciones");
        decorador_ConsultarOperaciones.iniciarTransaccion();
}
    
    public boolean verificarUsuario(String nombreUsuario){
        
        return decorador_ConsultarOperaciones.verificarUsuario(nombreUsuario);
        
    }
    
    public List<DTOConsultarOperacion> consultarOperacion(String nombreUsuario,String fechaDesde, String fechaHasta) throws ParseException{
        
        return decorador_ConsultarOperaciones.consultarOperacion(nombreUsuario, fechaDesde, fechaHasta);
    }
    
    
    
    
    
    
}
