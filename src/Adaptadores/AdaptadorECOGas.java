/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adaptadores;

import DTO.DTOComprobanteImpago;
import DTO.DTOComprobanteImpagoAtributo;
import DTO.DTOEcoGas;
import Empresas.EmpresaECOGas;
import Entidades.Atributo;
import Entidades.EmpresaTipoImpuesto;
import Entidades.EmpresaTipoImpuestoAtributo;
import Entidades.TipoImpuestoAtributo;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class AdaptadorECOGas implements AdaptadorEmpresa {
Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
       
    @Override
    public List<DTOComprobanteImpago> obtenerComprobantesImpagos(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto) {
List<DTOComprobanteImpago> list_dto_comprobante_impago = new ArrayList<>();  

EmpresaECOGas ecogas = new EmpresaECOGas();
List<DTOEcoGas> list_eco = ecogas.conectarEmpresaECOGAS(codigoPagoElectronico);
    
    if(list_eco.get(0).getImporte() != null){
    for(int j=0; j < list_eco.size();j++){ //POR CADA COMPROBANTEIMPAGO ENCONTRADO
           List<DTOComprobanteImpagoAtributo> list_dto_cia = new ArrayList<>(); 
           DTOEcoGas dto_ecogas = list_eco.get(j);
           DTOComprobanteImpago dto_comprobante_impago = new DTOComprobanteImpago();
            int numeroComprobante = Integer.parseInt(dto_ecogas.getNumeroComprobante());
            
            dto_comprobante_impago.setNumeroComprobanteImpago(numeroComprobante);
            
           
            List<EmpresaTipoImpuestoAtributo> empresaTIA = empresa_tipo_impuesto.getEmpresaTIA();
            
         
            for (int i = 0; i < empresaTIA.size(); i++) {
                 
                DTOComprobanteImpagoAtributo dto_comprobante_impago_atributo = new DTOComprobanteImpagoAtributo();
                EmpresaTipoImpuestoAtributo empresa_tia = empresaTIA.get(i);
                TipoImpuestoAtributo tipoimpuestoatributo = empresa_tia.getTipoimpuestoatributo();
                Atributo atributo =tipoimpuestoatributo.getAtributo();
                
         
                
                if("Importe".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_ecogas.getImporte());
                    
                }
                if("Fecha de vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_ecogas.getFechaVencimiento());
                    
                }
                
                
                list_dto_cia.add(dto_comprobante_impago_atributo);
                
                
            }
            
            dto_comprobante_impago.setList_dto_cia(list_dto_cia);
            list_dto_comprobante_impago.add(dto_comprobante_impago);
            
           
           
       }
     return list_dto_comprobante_impago;   
    } else {
        
        return null;
    }
        
            
  
      
    
    
    
    }

    @Override
    public DTOComprobanteImpago obtenerComprobanteImpago(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto, int numeroComprobante) {
            DTOEcoGas eco1 = new DTOEcoGas();
    if("123".equals(codigoPagoElectronico)){
    eco1.setImporte("500");
    eco1.setFechaVencimiento(formateador.format(ahora));
    eco1.setNumeroComprobante("1");
    }
    List<DTOEcoGas> list_eco = new ArrayList<>();
    list_eco.add(eco1);
    
       
        for(int j=0; j < list_eco.size();j++){ //POR CADA COMPROBANTEIMPAGO ENCONTRADO
           List<DTOComprobanteImpagoAtributo> list_dto_cia = new ArrayList<>(); 
           DTOEcoGas dto_ecogas = list_eco.get(j);
           DTOComprobanteImpago dto_comprobante_impago = new DTOComprobanteImpago();
            
           if(Integer.parseInt(dto_ecogas.getNumeroComprobante()) == numeroComprobante){ 
            
            dto_comprobante_impago.setNumeroComprobanteImpago(Integer.parseInt(dto_ecogas.getNumeroComprobante()));
           
            List<EmpresaTipoImpuestoAtributo> empresaTIA = empresa_tipo_impuesto.getEmpresaTIA();
            
         
            for (int i = 0; i < empresaTIA.size(); i++) {
                 
                DTOComprobanteImpagoAtributo dto_comprobante_impago_atributo = new DTOComprobanteImpagoAtributo();
                EmpresaTipoImpuestoAtributo empresa_tia = empresaTIA.get(i);
                TipoImpuestoAtributo tipoimpuestoatributo = empresa_tia.getTipoimpuestoatributo();
                Atributo atributo =tipoimpuestoatributo.getAtributo();
                
         
                
                if("Importe".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_ecogas.getImporte());
                    
                }
                if("Fecha de vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_ecogas.getFechaVencimiento());
                    
                }
                
                
                list_dto_cia.add(dto_comprobante_impago_atributo);
                
                
            }
            
            dto_comprobante_impago.setList_dto_cia(list_dto_cia);
            return dto_comprobante_impago;
     }
        
        }  
        
       
        
        return null;
    }
        
        
       public boolean informarPago(int numeroComprobante, double importeOperacion){
        
        
        
        
       return true; 
    } 
        
        
        }
    

