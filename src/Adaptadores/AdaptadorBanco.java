/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adaptadores;


import DTO.DTOCuenta;
import DTO.DTOSaldo;
import java.util.List;

/**
 *
 * @author Maxi
 */
public interface AdaptadorBanco {
    
    public DTOSaldo obtenerSaldoCuenta(int numeroCuenta);
  
    
}
