/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adaptadores;

import DTO.DTOComprobanteImpago;
import Entidades.EmpresaTipoImpuesto;
import Entidades.Operacion;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public interface AdaptadorEmpresa {
    
    List<DTOComprobanteImpago> obtenerComprobantesImpagos(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto);
    
    DTOComprobanteImpago obtenerComprobanteImpago(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto, int numeroComprobante);
    
    public boolean informarPago(int numeroComprobante, double importeOperacion);
}
