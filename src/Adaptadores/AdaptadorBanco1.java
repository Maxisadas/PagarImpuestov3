/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adaptadores;

import Banco.BancoNacion;
import DTO.DTOCuenta;
import DTO.DTODebitoEstado;
import DTO.DTODebitoEstadoBanco1;
import DTO.DTOSaldo;
import DTO.DTOSaldoBanco;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class AdaptadorBanco1 implements AdaptadorBanco {
    
    public DTOSaldo obtenerSaldoCuenta(int numeroCuenta){
        
        
        BancoNacion banconacion = new BancoNacion();
        
        //SE ESTABLECE LA CONEXION CON EL BANCO        
        DTOSaldoBanco dto_saldo_banco = banconacion.conexionObtenerSaldo(numeroCuenta);
        
        
            DTOSaldo dtoSaldo = new DTOSaldo();
            dtoSaldo.setSaldo(dto_saldo_banco.getSaldo());
            
            
        
        
        
        
        return dtoSaldo;
    }

    public DTODebitoEstado generarDebito(double importeOperacion, int numeroCuenta) {
        
        BancoNacion banco_nacion = new BancoNacion();
        
        DTODebitoEstadoBanco1 dto_debito = banco_nacion.conexionDebito(importeOperacion, numeroCuenta);
        
        DTODebitoEstado dto_debito_estado = new DTODebitoEstado();
        dto_debito_estado.setCodigoEstado(dto_debito.getCodigoEstado());
        dto_debito_estado.setMotivoEstado(dto_debito.getMotivoEstado());
        dto_debito_estado.setNombreEstado(dto_debito.getNombreEstado());
        
        return dto_debito_estado;
    }

    
    
    
}
