/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adaptadores;

import DTO.*;
import Empresas.EmpresaClaro;
import Entidades.Atributo;
import Entidades.EmpresaTipoImpuesto;
import Entidades.EmpresaTipoImpuestoAtributo;
import Entidades.Operacion;
import Entidades.TipoDato;
import Entidades.TipoImpuestoAtributo;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author phantom2024
 */
public class AdaptadorClaro implements AdaptadorEmpresa{

    @Override
    public List<DTOComprobanteImpago> obtenerComprobantesImpagos(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto) {
       List<DTOComprobanteImpago> list_dto_comprobante_impago = new ArrayList<>();
       EmpresaClaro empresa_claro = new EmpresaClaro(); 
       
       
           
      List<DTOClaro> list_dto_claro = empresa_claro.conectarEmpresaClaro(codigoPagoElectronico); //SE ESTABLECE LA CONEXION CON CLARO
        
      if(list_dto_claro != null){ 
       for(int j=0; j < list_dto_claro.size();j++){ //POR CADA COMPROBANTEIMPAGO ENCONTRADO
           List<DTOComprobanteImpagoAtributo> list_dto_cia = new ArrayList<>(); 
           DTOClaro dto_claro = list_dto_claro.get(j);
           DTOComprobanteImpago dto_comprobante_impago = new DTOComprobanteImpago();
            int numeroComprobante = Integer.parseInt(dto_claro.getNumeroComprobante());
            
            dto_comprobante_impago.setNumeroComprobanteImpago(numeroComprobante);
            
           
            List<EmpresaTipoImpuestoAtributo> empresaTIA = empresa_tipo_impuesto.getEmpresaTIA();
            
         
            for (int i = 0; i < empresaTIA.size(); i++) {
                 
                DTOComprobanteImpagoAtributo dto_comprobante_impago_atributo = new DTOComprobanteImpagoAtributo();
                EmpresaTipoImpuestoAtributo empresa_tia = empresaTIA.get(i);
                TipoImpuestoAtributo tipoimpuestoatributo = empresa_tia.getTipoimpuestoatributo();
                Atributo atributo =tipoimpuestoatributo.getAtributo();
                
         
                
                if("Importe".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getImporte());
                    
                }
                if("Fecha de vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getFacturaVencimiento());
                    
                }
                if("Fecha proximo vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getProximoVencimiento());
                    
                }
                if("Monto minimo exigible".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getMinimoMontoExigible());
                    
                }
                
                list_dto_cia.add(dto_comprobante_impago_atributo);
                
                
            }
            
            dto_comprobante_impago.setList_dto_cia(list_dto_cia);
            list_dto_comprobante_impago.add(dto_comprobante_impago);
            
           
           
       }
       
       return list_dto_comprobante_impago;
        
      }else{
          
           return null;
          
      }  
        
            
            
            
            
            
        

        
        
    }

    public DTOComprobanteImpago obtenerComprobanteImpago(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto,int numeroComprobante){
        
        EmpresaClaro empresa_claro = new EmpresaClaro(); 
       
        List<DTOClaro> list_dto_claro = empresa_claro.conectarEmpresaClaro(codigoPagoElectronico); //SE ESTABLECE LA CONEXION CON CLARO

        for(int j=0; j < list_dto_claro.size();j++){ //POR CADA COMPROBANTEIMPAGO ENCONTRADO
           List<DTOComprobanteImpagoAtributo> list_dto_cia = new ArrayList<>(); 
           DTOClaro dto_claro = list_dto_claro.get(j);
           DTOComprobanteImpago dto_comprobante_impago = new DTOComprobanteImpago();
            
           if(Integer.parseInt(dto_claro.getNumeroComprobante()) == numeroComprobante){ 
            
            dto_comprobante_impago.setNumeroComprobanteImpago(Integer.parseInt(dto_claro.getNumeroComprobante()));
           
            List<EmpresaTipoImpuestoAtributo> empresaTIA = empresa_tipo_impuesto.getEmpresaTIA();
            
         
            for (int i = 0; i < empresaTIA.size(); i++) {
                 
                DTOComprobanteImpagoAtributo dto_comprobante_impago_atributo = new DTOComprobanteImpagoAtributo();
                EmpresaTipoImpuestoAtributo empresa_tia = empresaTIA.get(i);
                TipoImpuestoAtributo tipoimpuestoatributo = empresa_tia.getTipoimpuestoatributo();
                Atributo atributo =tipoimpuestoatributo.getAtributo();
                
         
                
                if("Importe".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getImporte());
                    
                }
                if("Fecha de vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getFacturaVencimiento());
                    
                }
                if("Fecha proximo vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getProximoVencimiento());
                    
                }
                if("Monto minimo exigible".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getMinimoMontoExigible());
                    
                }
                
                list_dto_cia.add(dto_comprobante_impago_atributo);
                
                
            }
            
            dto_comprobante_impago.setList_dto_cia(list_dto_cia);
            return dto_comprobante_impago;
     }
        
        }  
        
        
        return null;
    }
    
    public boolean informarPago(int numeroComprobante, double importeOperacion){
        
        EmpresaClaro empresa_claro = new EmpresaClaro();
        boolean informeDebito = empresa_claro.conectarEmpresa(numeroComprobante, importeOperacion);
        
        
       return informeDebito; 
    }
    
    
    
}
