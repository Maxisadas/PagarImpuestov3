/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adaptadores;

import DTO.DTOMovistar;
import DTO.DTOComprobanteImpago;
import DTO.DTOComprobanteImpagoAtributo;
import Empresas.EmpresaMovistar;
import Entidades.Atributo;
import Entidades.EmpresaTipoImpuesto;
import Entidades.EmpresaTipoImpuestoAtributo;
import Entidades.Operacion;
import Entidades.TipoImpuestoAtributo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public class AdaptadorMovistar implements AdaptadorEmpresa{

    @Override
    public List<DTOComprobanteImpago> obtenerComprobantesImpagos(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto) {
        
    List<DTOComprobanteImpago> list_dto_comprobante_impago = new ArrayList<>();
       EmpresaMovistar empresa_movistar = new EmpresaMovistar(); 
       
       
           
      List<DTOMovistar> list_dto_movistar = empresa_movistar.conectarEmpresaMovistar(codigoPagoElectronico); //SE ESTABLECE LA CONEXION CON CLARO
        
       if(list_dto_movistar != null){
       for(int j=0; j < list_dto_movistar.size();j++){ //POR CADA COMPROBANTEIMPAGO ENCONTRADO
           List<DTOComprobanteImpagoAtributo> list_dto_cia = new ArrayList<>(); 
           DTOMovistar dto_movistar = list_dto_movistar.get(j);
           DTOComprobanteImpago dto_comprobante_impago = new DTOComprobanteImpago();
            int numeroComprobante = Integer.parseInt(dto_movistar.getNumeroComprobante());
            
            dto_comprobante_impago.setNumeroComprobanteImpago(numeroComprobante);
            
           
            List<EmpresaTipoImpuestoAtributo> empresaTIA = empresa_tipo_impuesto.getEmpresaTIA();
            
         
            for (int i = 0; i < empresaTIA.size(); i++) {
                 
                DTOComprobanteImpagoAtributo dto_comprobante_impago_atributo = new DTOComprobanteImpagoAtributo();
                EmpresaTipoImpuestoAtributo empresa_tia = empresaTIA.get(i);
                TipoImpuestoAtributo tipoimpuestoatributo = empresa_tia.getTipoimpuestoatributo();
                Atributo atributo =tipoimpuestoatributo.getAtributo();
                
         
                
                if("Importe".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_movistar.getImporte());
                    
                }
                if("Fecha de vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_movistar.getFacturaVencimiento());
                    
                }
                
                if("Monto minimo exigible".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_movistar.getMinimoMontoExigible());
                    
                }
                
                list_dto_cia.add(dto_comprobante_impago_atributo);
                
                
            }
            
            dto_comprobante_impago.setList_dto_cia(list_dto_cia);
            list_dto_comprobante_impago.add(dto_comprobante_impago);
    
    
    
    
    
    
    
        }
       return list_dto_comprobante_impago;
       }else{
           
           return null;
       }
    
     
    }
    
    public DTOComprobanteImpago obtenerComprobanteImpago(String codigoPagoElectronico, EmpresaTipoImpuesto empresa_tipo_impuesto, int numeroComprobante){
        
        EmpresaMovistar empresa_movistar = new EmpresaMovistar(); 
       
        List<DTOMovistar> list_dto_movistar = empresa_movistar.conectarEmpresaMovistar(codigoPagoElectronico); //SE ESTABLECE LA CONEXION CON CLARO

        for(int j=0; j < list_dto_movistar.size();j++){ //POR CADA COMPROBANTEIMPAGO ENCONTRADO
           List<DTOComprobanteImpagoAtributo> list_dto_cia = new ArrayList<>(); 
           DTOMovistar dto_claro = list_dto_movistar.get(j);
           DTOComprobanteImpago dto_comprobante_impago = new DTOComprobanteImpago();
            
           if(Integer.parseInt(dto_claro.getNumeroComprobante()) == numeroComprobante){ 
            
            dto_comprobante_impago.setNumeroComprobanteImpago(Integer.parseInt(dto_claro.getNumeroComprobante()));
           
            List<EmpresaTipoImpuestoAtributo> empresaTIA = empresa_tipo_impuesto.getEmpresaTIA();
            
         
            for (int i = 0; i < empresaTIA.size(); i++) {
                 
                DTOComprobanteImpagoAtributo dto_comprobante_impago_atributo = new DTOComprobanteImpagoAtributo();
                EmpresaTipoImpuestoAtributo empresa_tia = empresaTIA.get(i);
                TipoImpuestoAtributo tipoimpuestoatributo = empresa_tia.getTipoimpuestoatributo();
                Atributo atributo =tipoimpuestoatributo.getAtributo();
                
         
                
                if("Importe".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getImporte());
                    
                }
                if("Fecha de vencimiento".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getFacturaVencimiento());
                    
                }
                
                if("Monto minimo exigible".equals(atributo.getNombreAtributo())){
             dto_comprobante_impago_atributo.setNombreAtributo(atributo.getNombreAtributo());
             dto_comprobante_impago_atributo.setValor(dto_claro.getMinimoMontoExigible());
                    
                }
                
                list_dto_cia.add(dto_comprobante_impago_atributo);
                
                
            }
            
            dto_comprobante_impago.setList_dto_cia(list_dto_cia);
            return dto_comprobante_impago;
     }
        
        }
        
        return null;
    }
    
    public boolean informarPago(int numeroComprobante, double importeOperacion){
        
        EmpresaMovistar empresa_movistar = new EmpresaMovistar();
        boolean informeDebito = empresa_movistar.conectarEmpresa(numeroComprobante, importeOperacion);
        
        
       return informeDebito; 
    }
    
    
    
}