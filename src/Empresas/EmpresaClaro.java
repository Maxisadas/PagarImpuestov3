/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresas;

import DTO.DTOClaro;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class EmpresaClaro {
    
    public List<DTOClaro> conectarEmpresaClaro(String codigoPagoElectronico){
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        Date proximovencimiento = new Date();
        proximovencimiento.setDate(60);
        
        if("123".equals(codigoPagoElectronico)){
            
            List<DTOClaro> list_dto_claro = new ArrayList<>();
            DTOClaro dtoclaro1 = new DTOClaro();//primer comprobante
            dtoclaro1.setFacturaVencimiento(formateador.format(ahora));
            dtoclaro1.setImporte("500");
            dtoclaro1.setMinimoMontoExigible("100");
            dtoclaro1.setNumeroComprobante("1");
            dtoclaro1.setProximoVencimiento(formateador.format(proximovencimiento));
            
            DTOClaro dtoclaro2 = new DTOClaro(); //segundo comprobante
            dtoclaro2.setFacturaVencimiento(formateador.format(ahora));
            dtoclaro2.setImporte("350");
            dtoclaro2.setMinimoMontoExigible("75");
            dtoclaro2.setNumeroComprobante("2");
            dtoclaro2.setProximoVencimiento(formateador.format(proximovencimiento));
            
            DTOClaro dtoclaro3 = new DTOClaro(); //tercer comprobante
            dtoclaro3.setFacturaVencimiento(formateador.format(ahora));
            dtoclaro3.setImporte("600");
            dtoclaro3.setMinimoMontoExigible("125");
            dtoclaro3.setNumeroComprobante("3");
            dtoclaro3.setProximoVencimiento(formateador.format(proximovencimiento));
            
            list_dto_claro.add(dtoclaro1);
            list_dto_claro.add(dtoclaro2);
            list_dto_claro.add(dtoclaro3);
            return list_dto_claro;
        }else{
            
            
            return null;
        }
        
        
        
        
    }
    
    public boolean conectarEmpresa(int numeroComprobante, double importeOperacion){
        
        // ALGO HACE LA EMPRESA CON ESOS DATOS
        
        
        return true;
    }
    
    
    
    
    
}
