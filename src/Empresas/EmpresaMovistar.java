/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresas;


import DTO.DTOMovistar;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class EmpresaMovistar {
    
     public List<DTOMovistar> conectarEmpresaMovistar(String codigoPagoElectronico){
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        Date proximovencimiento = new Date();
        proximovencimiento.setDate(60);
        
        if("123".equals(codigoPagoElectronico)){
            
            List<DTOMovistar> list_dto_movistar = new ArrayList<>();
            DTOMovistar dtomovistar1 = new DTOMovistar();//primer comprobante
            dtomovistar1.setFacturaVencimiento(formateador.format(ahora));
            dtomovistar1.setImporte("250");
            dtomovistar1.setMinimoMontoExigible("50");
            dtomovistar1.setNumeroComprobante("1");
            
            
            DTOMovistar dtomovistar2 = new DTOMovistar(); //segundo comprobante
            dtomovistar2.setFacturaVencimiento(formateador.format(ahora));
            dtomovistar2.setImporte("150");
            dtomovistar2.setMinimoMontoExigible("42");
            dtomovistar2.setNumeroComprobante("2");
            
            
            DTOMovistar dtomovistar3 = new DTOMovistar(); //tercer comprobante
            dtomovistar3.setFacturaVencimiento(formateador.format(ahora));
            dtomovistar3.setImporte("321");
            dtomovistar3.setMinimoMontoExigible("63");
            dtomovistar3.setNumeroComprobante("3");
            
            DTOMovistar dtomovistar4 = new DTOMovistar(); //cuarto comprobante
            dtomovistar4.setFacturaVencimiento(formateador.format(ahora));
            dtomovistar4.setImporte("420");
            dtomovistar4.setMinimoMontoExigible("100");
            dtomovistar4.setNumeroComprobante("4");
            
            DTOMovistar dtomovistar5 = new DTOMovistar(); //quinto comprobante
            dtomovistar5.setFacturaVencimiento(formateador.format(ahora));
            dtomovistar5.setImporte("532");
            dtomovistar5.setMinimoMontoExigible("125");
            dtomovistar5.setNumeroComprobante("5");
            
            
            list_dto_movistar.add(dtomovistar1);
            list_dto_movistar.add(dtomovistar2);
            list_dto_movistar.add(dtomovistar3);
            list_dto_movistar.add(dtomovistar4);
            list_dto_movistar.add(dtomovistar5);
            return list_dto_movistar;
        }else{
            
            
            return null;
        }
    
    }
     public boolean conectarEmpresa(int numeroComprobante, double importeOperacion){
        
        // ALGO HACE LA EMPRESA CON ESOS DATOS
        
        
        return true;
    }
}
