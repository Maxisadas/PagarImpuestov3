/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTONuevo;

import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author Lorenzo
 */
public class DTOTipoImpuestoAlta {
    public String nombre;
    public int codigo;
    public boolean modificable;
    public Date fechaHabilitacion;
    public Date fechaInhabilitacion;
        public ArrayList<DTOAtributoTipoImpuestoAlta> dtoAtributo = new ArrayList<DTOAtributoTipoImpuestoAlta>();

    public DTOTipoImpuestoAlta() {
    }

    public DTOTipoImpuestoAlta(String nombre, int codigo, boolean modificable,Date fechaHabilitacion,Date fechaInhabilitacion) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.modificable = modificable;
        this.fechaInhabilitacion = fechaInhabilitacion;
        this.fechaHabilitacion = fechaHabilitacion;
        
    }
}
