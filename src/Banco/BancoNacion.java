/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import DTO.*;

/**
 *
 * @author Maxi
 */
public class BancoNacion {
    
    public DTOSaldoBanco conexionObtenerSaldo(int numeroCuenta){
        
        if(numeroCuenta == 1){
            
            DTOSaldoBanco dto_saldo_banco = new DTOSaldoBanco();
            dto_saldo_banco.setSaldo(1000);
            return dto_saldo_banco;
            
        }
        if(numeroCuenta == 2){
            
            DTOSaldoBanco dto_saldo_banco = new DTOSaldoBanco();
            dto_saldo_banco.setSaldo(200);
            return dto_saldo_banco;
            
        }
        
        return null;
        
    }
    
    public DTODebitoEstadoBanco1 conexionDebito(double importeOperacion, int numeroCuenta){
        
        DTODebitoEstadoBanco1 dto_debito_estado_banco1 = new DTODebitoEstadoBanco1();
        DTOSaldoBanco dto_saldo_banco = null;
        if(numeroCuenta == 1){
            
            dto_saldo_banco = new DTOSaldoBanco();
            dto_saldo_banco.setSaldo(1000);
            
            
        }
        if(numeroCuenta == 2){
            
            dto_saldo_banco = new DTOSaldoBanco();
            dto_saldo_banco.setSaldo(200);
            
            
        }
        
        if ((dto_saldo_banco.getSaldo() - importeOperacion) >= 0) {
            
            dto_debito_estado_banco1.setCodigoEstado(01);
            dto_debito_estado_banco1.setNombreEstado("Debitado");
            dto_debito_estado_banco1.setMotivoEstado("Su pago se débito con exito");
            
        }else{
            
            dto_debito_estado_banco1.setCodigoEstado(02);
            dto_debito_estado_banco1.setNombreEstado("No debitado");
            dto_debito_estado_banco1.setMotivoEstado("No se ha podido debitar de su cuenta.");
            
        }

        
        return dto_debito_estado_banco1;
        
    }
    
    
    
    
}
