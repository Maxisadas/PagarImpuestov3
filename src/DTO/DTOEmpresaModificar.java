/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;
import Entidades.*;
import java.util.Date;
/**
 *
 * @author Emanuel
 */
public class DTOEmpresaModificar {
    private String nombreEmpresa;
    private String cuitEmpresa;
    private Date fechaHabEmpresa;
    private Date fechaInhabEmpresa;
    private TipoEmpresa tipoEmpresa;
   
    
    public DTOEmpresaModificar(){
        
    }
    public DTOEmpresaModificar(String nombre, String cuit, Date fechaHab,Date fechaInhab, TipoEmpresa tipo){        
        this.cuitEmpresa = cuit;
        this.nombreEmpresa = nombre;
        this.fechaHabEmpresa = fechaHab;
        this.fechaInhabEmpresa = fechaInhab;
        this.tipoEmpresa = tipo;
  
    }
    
    public String getCuitEmpresa(){
        return cuitEmpresa;
    }
    public void setCuitEmpresa(String cuit){
        this.cuitEmpresa = cuit;
    }
    public String getNombreEmpresa(){
        return nombreEmpresa;
    }
    public void setNombreEmpresa(String nombre){
        this.nombreEmpresa = nombre;
    }
     public Date getFechaHab(){
        return fechaHabEmpresa;
    }
    public void setFechaHab(Date fechaHab){
        this.fechaHabEmpresa = fechaHab;
    }
     public Date getFechaInhab(){
        return fechaInhabEmpresa;
    }
    public void setFechaInhab(Date fechaInhab){
        this.fechaInhabEmpresa = fechaInhab;
    }
     public TipoEmpresa getTipoEmpresa(){
        return tipoEmpresa;
    }
    public void setTipoEmpresa(TipoEmpresa tipo){
        this.tipoEmpresa = tipo;
    }

}
