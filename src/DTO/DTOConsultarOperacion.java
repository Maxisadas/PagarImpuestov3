/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class DTOConsultarOperacion {
    
    
    
    private String apellidoCliente;
    private String cuitEmpresa;
    private int dniCliente;
    private Date fechaOperacion;
    private Date fechaVencimiento;
    private double importeOperacion;

    public double getImporteOperacion() {
        return importeOperacion;
    }

    public void setImporteOperacion(double importeOperacion) {
        this.importeOperacion = importeOperacion;
    }
    private String nombrecliente;
    private String nombreEmpresa;
    private String nombreTipoImpuesto;
    private int numeroComprobante;
    private int numeroCuenta;
    private int numeroOperacion;

    public int getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(int numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public String getCuitEmpresa() {
        return cuitEmpresa;
    }

    public void setCuitEmpresa(String cuitEmpresa) {
        this.cuitEmpresa = cuitEmpresa;
    }
    
    private List<DTOListOperacionAtributo> list_operacion_atributo;

    public List<DTOListOperacionAtributo> getList_operacion_atributo() {
        return list_operacion_atributo;
    }

    public void setList_operacion_atributo(List<DTOListOperacionAtributo> list_operacion_atributo) {
        this.list_operacion_atributo = list_operacion_atributo;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

   

    public int getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(int dniCliente) {
        this.dniCliente = dniCliente;
    }

    public Date getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

   

    

    public String getNombrecliente() {
        return nombrecliente;
    }

    public void setNombrecliente(String nombrecliente) {
        this.nombrecliente = nombrecliente;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getNombreTipoImpuesto() {
        return nombreTipoImpuesto;
    }

    public void setNombreTipoImpuesto(String nombreTipoImpuesto) {
        this.nombreTipoImpuesto = nombreTipoImpuesto;
    }

    public int getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(int numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    
    
    
    
    
}
