/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import Entidades.TipoImpuestoAtributo;

/**
 *
 * @author phantom2024
 */
public class DTOComprobanteImpagoAtributo {
    
    private String valor;
    private String nombreAtributo;

    public String getNombreAtributo() {
        return nombreAtributo;
    }

    public void setNombreAtributo(String nombreAtributo) {
        this.nombreAtributo = nombreAtributo;
    }



    public DTOComprobanteImpagoAtributo() {
    }

    
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
