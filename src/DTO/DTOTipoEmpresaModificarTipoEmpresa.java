/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;
import java.util.Date;

public class DTOTipoEmpresaModificarTipoEmpresa {
     private String nombreTipoEmpresa;
    private int codigoTipoEmpresa;
    private Date fechaHabTipoEmpresa;
    private Date fechaInhabTipoEmpresa;
    
    public DTOTipoEmpresaModificarTipoEmpresa(){
        
    }
    public DTOTipoEmpresaModificarTipoEmpresa(int codigoTipoEmpresa, String nombreTipoEmpresa, Date fechaHabTipoEmpresa, Date fechaInhabTipoEmpresa){
        this.codigoTipoEmpresa = codigoTipoEmpresa;
        this.nombreTipoEmpresa = nombreTipoEmpresa;
        this.fechaHabTipoEmpresa = fechaHabTipoEmpresa;
        this.fechaInhabTipoEmpresa = fechaInhabTipoEmpresa;
    }
    
     public String getNombre() {
        return nombreTipoEmpresa;
    }

    public void setNombre(String nombre) {
        this.nombreTipoEmpresa = nombre;
    }

    public int getCodigo() {
        return codigoTipoEmpresa;
    }

    public void setCodigo(int codigo) {
        this.codigoTipoEmpresa = codigo;
    }
    public Date getFechaHab() {
        return fechaHabTipoEmpresa;
    }

    public void setFechaHab(Date fechaHab) {
        this.fechaHabTipoEmpresa = fechaHab;
    }

    public Date getFechaInhab() {
        return fechaInhabTipoEmpresa;
    }

    public void setFechaInhab(Date fechaInhab) {
        this.fechaInhabTipoEmpresa = fechaInhab;
    }
}
