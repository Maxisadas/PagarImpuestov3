/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author phantom2024
 */
public class DTOClaro {
    
    private String minimoMontoExigible;
    private String facturaVencimiento;
    private String importe;
    private String numeroComprobante;
    private String proximoVencimiento;

    public String getMinimoMontoExigible() {
        return minimoMontoExigible;
    }

    public void setMinimoMontoExigible(String minimoMontoExigible) {
        this.minimoMontoExigible = minimoMontoExigible;
    }

    public String getFacturaVencimiento() {
        return facturaVencimiento;
    }

    public void setFacturaVencimiento(String facturaVencimiento) {
        this.facturaVencimiento = facturaVencimiento;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public String getProximoVencimiento() {
        return proximoVencimiento;
    }

    public void setProximoVencimiento(String proximoVencimiento) {
        this.proximoVencimiento = proximoVencimiento;
    }

    public DTOClaro() {
    }

   
    
    
}
