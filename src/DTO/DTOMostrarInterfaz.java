/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Maxi
 */
public class DTOMostrarInterfaz {
    
    boolean modificable;

    public boolean isModificable() {
        return modificable;
    }

    public void setModificable(boolean modificable) {
        this.modificable = modificable;
    }
    
    private double importeOperacion;

    public double getImporteOperacion() {
        return importeOperacion;
    }

    public void setImporteOperacion(double importeOperacion) {
        this.importeOperacion = importeOperacion;
    }
    private String nombreTipoCuenta;
    private String nombreEmpresa;
    private String nombreTipoImpuesto;
    private int numeroCuenta;
    private int numeroComprobante;
    private double saldoCuenta;

    

    public String getNombreTipoCuenta() {
        return nombreTipoCuenta;
    }

    public void setNombreTipoCuenta(String nombreTipoCuenta) {
        this.nombreTipoCuenta = nombreTipoCuenta;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getNombreTipoImpuesto() {
        return nombreTipoImpuesto;
    }

    public void setNombreTipoImpuesto(String nombreTipoImpuesto) {
        this.nombreTipoImpuesto = nombreTipoImpuesto;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public int getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(int numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public double getSaldoCuenta() {
        return saldoCuenta;
    }

    public void setSaldoCuenta(double saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
    }
    
    private double minimoImporteExigible;

    public double getMinimoImporteExigible() {
        return minimoImporteExigible;
    }

    public void setMinimoImporteExigible(double minimoImporteExigible) {
        this.minimoImporteExigible = minimoImporteExigible;
    }
    
}
