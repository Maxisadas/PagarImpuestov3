/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;
import java.util.Date;
/**
 *
 * @author Emanuel
 */
public class DTOTipoEmpresaBaja {
     private String nombreTipoEmpresa;
    private int codigoTipoEmpresa;
    private Date fechaHab;
    
    public DTOTipoEmpresaBaja() {
    }

    public DTOTipoEmpresaBaja(String nombre, int codigo,Date fechahab) {
        this.nombreTipoEmpresa = nombre;
        this.codigoTipoEmpresa = codigo;
        this.fechaHab = fechahab;
    }
    
     /**
     *
     * @return
     */
    public String getNombre() {
        return nombreTipoEmpresa;
    }

    public void setNombre(String nombre) {
        this.nombreTipoEmpresa = nombre;
    }

    public int getCodigo() {
        return codigoTipoEmpresa;
    }
    
    public void setCodigo(int codigo){
        this.codigoTipoEmpresa = codigo;
    }
    
    public Date getFechaHab(){
        return fechaHab;
    }
    
    public void setFechaHab(Date fecha){
        this.fechaHab = fecha;
    }
}
