/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Fernando
 */
public class DTOAtributo {
    private String nombre;
    private int codigo;
    private DTOTipoDato dtoTD;

    public DTOAtributo(String nombre, int codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public DTOAtributo() {
    }

    public DTOAtributo(String nombre, int codigo, DTOTipoDato dtoTD) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.dtoTD = dtoTD;
    }

    public DTOTipoDato getDtoTD() {
        return dtoTD;
    }

    public void setDtoTD(DTOTipoDato dtoTD) {
        this.dtoTD = dtoTD;
    }
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
