/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Maxi
 */
public class DTOListOperacionAtributo {
    
    private String nombreAtributo;
    private String valorOperacionAtributo;

    public String getNombreAtributo() {
        return nombreAtributo;
    }

    public void setNombreAtributo(String nombreAtributo) {
        this.nombreAtributo = nombreAtributo;
    }

    public String getValorOperacionAtributo() {
        return valorOperacionAtributo;
    }

    public void setValorOperacionAtributo(String valorOperacionAtributo) {
        this.valorOperacionAtributo = valorOperacionAtributo;
    }
    
    
}
