/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Entidades.Empresa;
import Entidades.TipoImpuesto;
import java.util.Date;

/**
 *
 * @author Cyntia
 */
public class PruebaGenerarTablas {

    public static void main(String[] args) {

        //Con esto solamente refresco el modelo
        HibernateUtil.getSessionFactory().openSession();

        try {
            HibernateUtil.getSession().beginTransaction();

            //desde aca puedo ir cargando la base de datos
            Empresa e = new Empresa();
            e.setNombreEmpresa("Claro");
            e.setCuitEmpresa("11223344");
            e.setFechaHabilitacionEmpresa(new Date());

            Empresa e2 = new Empresa();
            e2.setNombreEmpresa("Edemsa");
            e2.setCuitEmpresa("24524524527");
            e2.setFechaHabilitacionEmpresa(new Date());
            
            
            
             //Carga PAra tipo de Empresa
              TipoImpuesto tpservicios = new TipoImpuesto();
              tpservicios.setNombreTipoImpuesto("Internet");
              tpservicios.setcodigoTipoImpuesto(1);

              TipoImpuesto tpnoservicios = new TipoImpuesto();
              tpservicios.setNombreTipoImpuesto("Gas");
              tpservicios.setcodigoTipoImpuesto(2);
        
            
              
              
              
            
            
            
            
            HibernateUtil.getSession().save(e);
            HibernateUtil.getSession().save(e2);
            
            HibernateUtil.getSession().getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error al guardar empresa "+e.getMessage());
            HibernateUtil.getSession().getTransaction().rollback();
        }

    }
}
