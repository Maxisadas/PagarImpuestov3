/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Cyntia
 */
public class Comision extends Entidad{
    private double valorComision;
    
    private Operacion operacion;

    public Comision() {
          super();
    }

    public double getValorComision() {
        return valorComision;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public void setValorComision(double valorComision) {
        this.valorComision = valorComision;
    }
    @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }
}
