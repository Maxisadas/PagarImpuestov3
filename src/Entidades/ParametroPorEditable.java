/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Cyntia
 */
public class ParametroPorEditable extends Entidad{
    private double porcentajeCon;
    private double porcentajeSin;

    public ParametroPorEditable(double porcentajeCon) {
        this.porcentajeCon = porcentajeCon;
    }

    public double getPorcentajeCon() {
        return porcentajeCon;
    }

    public void setPorcentajeCon(double porcentajeCon) {
        this.porcentajeCon = porcentajeCon;
    }

    public double getPorcentajeSin() {
        return porcentajeSin;
    }

    public void setPorcentajeSin(double porcentajeSin) {
        this.porcentajeSin = porcentajeSin;
    }
    
      @Override
    public String getOID() {
        return super.OID;
    }    
    @Override
    public void setOID(String oidEmpresa) {
        super.OID = oidEmpresa;
    }

    public ParametroPorEditable() {
          super();
    }
    
}
