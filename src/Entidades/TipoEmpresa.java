/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;
import java.util.Date;


public class TipoEmpresa extends Entidad{
    private int codigoTipoEmpresa;
    private String nombreTipoEmpresa;
    private Date fechaHabTipoEmpresa;
    private Date fechaInhabTipoEmpresa;

    public TipoEmpresa() {
        super();
    }

    public TipoEmpresa(String OID) {
        super(OID);
    }

    public TipoEmpresa(int codigoTipoEmpresa, String nombreTipoEmpresa, Date fechaHabTipoEmpresa, Date fechaInhabTipoEmpresa, String OID) {
        super(OID);
        this.codigoTipoEmpresa = codigoTipoEmpresa;
        this.nombreTipoEmpresa = nombreTipoEmpresa;
        this.fechaHabTipoEmpresa = fechaHabTipoEmpresa;
        this.fechaInhabTipoEmpresa = fechaInhabTipoEmpresa;
    }

    @Override
    public String getOID() {
        return super.OID;
    }

    @Override
    public void setOID(String OID) {
        super.OID = OID;
    }

    public int getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setCodigoTipoEmpresa(int codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public String getNombreTipoEmpresa() {
        return nombreTipoEmpresa;
    }

    public void setNombreTipoEmpresa(String nombreTipoEmpresa) {
        this.nombreTipoEmpresa = nombreTipoEmpresa;
    }

    public Date getFechaHabTipoEmpresa() {
        return fechaHabTipoEmpresa;
    }

    public void setFechaHabTipoEmpresa(Date fechaHabTipoEmpresa) {
        this.fechaHabTipoEmpresa = fechaHabTipoEmpresa;
    }

    public Date getFechaInhabTipoEmpresa() {
        return fechaInhabTipoEmpresa;
    }

    public void setFechaInhabTipoEmpresa(Date fechaInhabTipoEmpresa) {
        this.fechaInhabTipoEmpresa = fechaInhabTipoEmpresa;
    }
    
}
