/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;

/**
 *
 * @author Maxi
 */
public class SistemaBanco extends Entidad {
        
        private int codigoSistemaBanco;
        private Date fechaHabilitacionSistemaBanco;
        private Date fechaInhabilitacionSistemaBanco;
        private Banco banco;
        private ParametroConexion parametroConexion;

    public ParametroConexion getParametroConexion() {
        return parametroConexion;
    }

    public void setParametroConexion(ParametroConexion parametroConexion) {
        this.parametroConexion = parametroConexion;
    }

    public int getCodigoSistemaBanco() {
        return codigoSistemaBanco;
    }

    public void setCodigoSistemaBanco(int codigoSistemaBanco) {
        this.codigoSistemaBanco = codigoSistemaBanco;
    }

    public Date getFechaHabilitacionSistemaBanco() {
        return fechaHabilitacionSistemaBanco;
    }

    public void setFechaHabilitacionSistemaBanco(Date fechaHabilitacionSistemaBanco) {
        this.fechaHabilitacionSistemaBanco = fechaHabilitacionSistemaBanco;
    }

    public Date getFechaInhabilitacionSistemaBanco() {
        return fechaInhabilitacionSistemaBanco;
    }

    public void setFechaInhabilitacionSistemaBanco(Date fechaInhabilitacionSistemaBanco) {
        this.fechaInhabilitacionSistemaBanco = fechaInhabilitacionSistemaBanco;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    @Override
    public String getOID() {
        return super.OID;
    }

    @Override
    public void setOID(String OID) {
       super.OID = OID;
   }
        
        
        
    
}
