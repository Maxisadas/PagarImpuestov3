/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControlLogin;

import DTO.*;
import Entidades.Usuario;
import Persistencia.FachadaPersistencia;
import java.util.*;

/**
 *
 * @author tinch
 */
public class ExpertoIniciarSesion {

    @SuppressWarnings("empty-statement")
    public boolean iniciarSesion(String user, String password) {
            List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
            DTOCriterio dtoCriterio = new DTOCriterio();
            dtoCriterio.setAtributo("nombreUsuario");
            dtoCriterio.setOperador("=");
            dtoCriterio.setValor(user);
            listaDTOCriterio.add(dtoCriterio);
            DTOCriterio dtoCriterio2 = new DTOCriterio();
            dtoCriterio2.setAtributo("contraseñaUsuario");
            dtoCriterio2.setOperador("=");
            dtoCriterio2.setValor(password);
            listaDTOCriterio.add(dtoCriterio2);
            List<Object> listaUsuario = FachadaPersistencia.getInstancia().buscar("Usuario", listaDTOCriterio);
            
            if(listaUsuario.size()>0){
             Usuario usuario = (Usuario) listaUsuario.get(0);   
                return true;
                
            }else{
                
                return false;
            }
            
  }    
}