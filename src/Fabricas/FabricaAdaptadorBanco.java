/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fabricas;

import Adaptadores.AdaptadorBanco;
import Adaptadores.AdaptadorBanco1;
import DTO.DTOCriterio;
import Entidades.ParametroConexion;
import Entidades.SistemaBanco;
import Persistencia.FachadaPersistencia;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Maxi
 */
public class FabricaAdaptadorBanco {
    
    
    private static FabricaAdaptadorBanco instancia;
    
    public static FabricaAdaptadorBanco getInstancia (){
        if (instancia == null){
            instancia = new FabricaAdaptadorBanco();
        }
        return instancia;
    }
    
    
        public AdaptadorBanco obtenerAdaptadorBanco(){
        DTOCriterio c10 = new DTOCriterio();
        c10.setAtributo("fechaInhabilitacionSistemaBanco");
        c10.setOperador("is null");
        c10.setValor(null);
        
        List<DTOCriterio> l10 = new ArrayList<>();
        l10.add(c10);
        
        SistemaBanco sistema_banco =(SistemaBanco) FachadaPersistencia.getInstancia().buscar("SistemaBanco", l10).get(0);
        ParametroConexion parametro_conexion = sistema_banco.getParametroConexion();
        if("1".equals(parametro_conexion.getCodigoConexion())){
            return new AdaptadorBanco1();
        } else { return null;
        }
        
        
        
    }
    
}
