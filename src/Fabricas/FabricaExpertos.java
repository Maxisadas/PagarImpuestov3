package Fabricas;

import ControlConsultarOperaciones.DecoradorConsultarOperaciones;
import ControlLogin.DecoradorIniciarSesion;
import ControlPagarImpuesto.DecoradorPagarImpuesto;

public class FabricaExpertos{
    private static FabricaExpertos instancia;
    
    public FabricaExpertos (){}
    
    public static FabricaExpertos getInstancia (){
        if (instancia == null){
            instancia = new FabricaExpertos();
        }
        return instancia;
    }

    public Object crearExperto (String casoDeUso){
        switch (casoDeUso)
        {
            case "PagarImpuesto":
                return new DecoradorPagarImpuesto();
            case "Iniciar Sesión":
                return new DecoradorIniciarSesion();
            case "Consultar Operaciones":
                return new DecoradorConsultarOperaciones();    
            default:
                return null;
        }
    }
}