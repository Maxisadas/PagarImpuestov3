package ControlPagarImpuesto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Adaptadores.AdaptadorBanco;
import Adaptadores.AdaptadorBanco1;
import Adaptadores.AdaptadorEmpresa;
import Fabricas.FabricaAdaptadorBanco;
import Fabricas.FabricaAdaptadorEmpresa;
import Entidades.*;
import DTO.*;
import Persistencia.FachadaPersistencia;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.text.html.parser.DTD;

/**
 *
 * @author phantom2024
 */
public class ExpertoPagarImpuesto {

    private Operacion operacion;
    List<ControlSaldo> list_control_saldo = new ArrayList<>();
    double saldoControl;
    

    public List<DTOTipoImpuesto> pagarImpuesto(String nombreUsuario) {
        boolean permiso = new Boolean(true);
        DTOCriterio c1 = new DTOCriterio();
        c1.setAtributo("nombreUsuario");
        c1.setOperador("=");
        c1.setValor(nombreUsuario);
        List<DTOCriterio> l1 = new ArrayList<>();
        l1.add(c1);
        Usuario usuario = (Usuario) FachadaPersistencia.getInstancia().buscar("Usuario",l1).get(0);



        List<DTOTipoImpuesto> lista_dto_ti = new ArrayList<>();

        Rol rol = usuario.getRol();
        List<RolOpcion> lista_rol_opcion = rol.getListRolOpcion();

        for (int i = 0; i < lista_rol_opcion.size(); i++) {
            if (lista_rol_opcion.get(i).getNombreRolOpcion().equals("CUPAGARIMPUESTO")) {
                
                
                permiso = true;
                Cliente cliente = usuario.getCliente();

                operacion = new Operacion();

                operacion.setCliente(cliente);

                DTOCriterio c2 = new DTOCriterio("fechaInhabilitacionTipoImpuesto", "is null", null);

                List<DTOCriterio> l2 = new ArrayList<>();
                l2.add(c2);

                List<Object> list_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("TipoImpuesto", l2);

                for (int u = 0; u < list_tipo_impuesto.size(); u++) {

                    TipoImpuesto tipoimpuesto = (TipoImpuesto) list_tipo_impuesto.get(u);

                    DTOTipoImpuesto dto_tipo_impuesto = new DTOTipoImpuesto();
                    dto_tipo_impuesto.setCodigo(tipoimpuesto.getcodigoTipoImpuesto());
                    dto_tipo_impuesto.setNombre(tipoimpuesto.getNombreTipoImpuesto());

                    lista_dto_ti.add(dto_tipo_impuesto);

                }

           }else{
               permiso = false; 
                
            }
        }
        
        if(permiso){
            
            return lista_dto_ti;
            
        }else{
            
           JOptionPane.showMessageDialog(null, "El Usuario no puede ejecutar esta opción, comuníquese con el Administrador", "No tiene permisos", 0);
               System.exit(0);
               return lista_dto_ti;
        }

        
            
            
            
        
        

        

    }

    public List<DTOEmpresa> seleccionarTipoImpuesto(int codigoTipoImpuesto) {

        List<DTOCriterio> l5 = new ArrayList<>();
        DTOCriterio c6 = new DTOCriterio();
        c6.setAtributo("codigoTipoImpuesto");
        c6.setOperador("=");
        c6.setValor(codigoTipoImpuesto);
        l5.add(c6);

        List<Object> list_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("TipoImpuesto", l5);
        TipoImpuesto tipo_impuesto = (TipoImpuesto) list_tipo_impuesto.get(0);
        operacion.setTipoImpuesto(tipo_impuesto);

        List<DTOCriterio> l3 = new ArrayList<>();

        DTOCriterio c3 = new DTOCriterio();
        c3.setAtributo("fechaInhabilitacionEmpresaTipoImpuesto");
        c3.setOperador("is null");
        c3.setValor(null);
        l3.add(c3);

        DTOCriterio c4 = new DTOCriterio();
        c4.setAtributo("tipoImpuesto");
        c4.setOperador("=");
        c4.setValor(tipo_impuesto);
        l3.add(c4);

        List<Object> list_empresa_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("EmpresaTipoImpuesto", l3);
        if (list_empresa_tipo_impuesto.size() > 0) {//SI LA LISTA ESTA VACIA
            List<DTOEmpresa> lista_dto_empresa = new ArrayList<>();

            for (int i = 0; i < list_empresa_tipo_impuesto.size(); i++) {

                EmpresaTipoImpuesto empresa_tipo_impuesto = (EmpresaTipoImpuesto) list_empresa_tipo_impuesto.get(i);
                DTOEmpresa dto_empresa = new DTOEmpresa();

                dto_empresa.setCodigoEmpresaTipoImpuesto(empresa_tipo_impuesto.getCodigoEmpresaTipoImpuesto());
                dto_empresa.setNombreEmpresa(empresa_tipo_impuesto.getEmpresa().getNombreEmpresa());

                lista_dto_empresa.add(dto_empresa);

            }

            return lista_dto_empresa;
        } else {

            return null;
        }
    }

    public void seleccionarEmpresa(int codigoEmpresaTipoImpuesto) {

        List<DTOCriterio> l4 = new ArrayList<>();
        DTOCriterio c5 = new DTOCriterio();
        c5.setAtributo("codigoEmpresaTipoImpuesto");
        c5.setOperador("=");
        c5.setValor(codigoEmpresaTipoImpuesto);
        l4.add(c5);

        List<Object> list_empresa_tipo_impuesto = FachadaPersistencia.getInstancia().buscar("EmpresaTipoImpuesto", l4);
        EmpresaTipoImpuesto empresa_tipo_impuesto = (EmpresaTipoImpuesto) list_empresa_tipo_impuesto.get(0);

        operacion.setEmpresaTipoImpuesto(empresa_tipo_impuesto);

    }

    public List<DTOComprobanteImpago> ingresarCodPagoElectronico(String codigoPagoElectronico) {

        operacion.setCodPagoElect(codigoPagoElectronico);

        EmpresaTipoImpuesto empresa_tipo_impuesto = operacion.getEmpresaTipoImpuesto();
        Empresa empresa = empresa_tipo_impuesto.getEmpresa();

        AdaptadorEmpresa adaptador_empresa = FabricaAdaptadorEmpresa.getInstancia().obtenerAdaptadorEmpresa(empresa);

    
            List<DTOComprobanteImpago> list_dto_comprobante_impago = adaptador_empresa.obtenerComprobantesImpagos(codigoPagoElectronico, empresa_tipo_impuesto);
            
   
        return list_dto_comprobante_impago;

    }

    public List<DTOCuenta> seleccionarNumeroCuenta(int numeroComprobante) {
        
        EmpresaTipoImpuesto empresa_tipo_impuesto = operacion.getEmpresaTipoImpuesto();
        Empresa empresa = operacion.getEmpresaTipoImpuesto().getEmpresa();
        String codigoPagoElec = operacion.getcodPagoElect();
        List<OperacionAtributo> list_operacion_atributo = new ArrayList<>();
        AdaptadorEmpresa adaptador_empresa = FabricaAdaptadorEmpresa.getInstancia().obtenerAdaptadorEmpresa(empresa);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //ADAPTADOR
  
            
            DTOComprobanteImpago dto_comprobante_impago = adaptador_empresa.obtenerComprobanteImpago(codigoPagoElec, empresa_tipo_impuesto, numeroComprobante);
            List<DTOComprobanteImpagoAtributo> list_comprobante_impago_atributo = dto_comprobante_impago.getList_dto_cia();
            operacion.setNumeroComprobante(dto_comprobante_impago.getNumeroComprobanteImpago());
            for (int i = 0; i < list_comprobante_impago_atributo.size(); i++) {

                String nombreAtributo = list_comprobante_impago_atributo.get(i).getNombreAtributo();
                
                // SE HACE EL CASTEO CORRESPONDIENTE POR CADA ATRIBUTO        
                if ("Importe".equals(nombreAtributo)) {
                    
                    if ("double".equals(empresa_tipo_impuesto.getEmpresaTIA().get(i).getTipoimpuestoatributo().getAtributo().getTipoDato().getNombreTipoDato())) {
                        double importeOperacion = Double.parseDouble(list_comprobante_impago_atributo.get(i).getValor());

                        operacion.setImporteOperacion(importeOperacion);
                    }

                } else if ("Fecha de vencimiento".equals(nombreAtributo)) {
                    if ("Date".equals(empresa_tipo_impuesto.getEmpresaTIA().get(i).getTipoimpuestoatributo().getAtributo().getTipoDato().getNombreTipoDato())) {
                        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
                        Date fecha_de_vencimiento = null;

                        try {
                            fecha_de_vencimiento = formatoDelTexto.parse(list_comprobante_impago_atributo.get(i).getValor());
                        } catch (ParseException ex) {
                            System.out.println(ex);
                        }

                        operacion.setFechaVencimientoComprobanteImpago(fecha_de_vencimiento);
                    }
                } else {
                    OperacionAtributo operacionAtributo = new OperacionAtributo();
                    operacionAtributo.setTipoImpuestoAtributo(empresa_tipo_impuesto.getEmpresaTIA().get(i).getTipoimpuestoatributo());
                    operacionAtributo.setValorOperacionAtibuto(list_comprobante_impago_atributo.get(i).getValor());
                    list_operacion_atributo.add(operacionAtributo);
                }

            }
            operacion.setListOperacionAtributo(list_operacion_atributo);
            ///////////////////////////////////////////////////////////////////////////////////////////////// 
            // SE HACE LA BUSQUEDA DE LAS CUENTAS

            Cliente cliente = operacion.getCliente();
            DTOCriterio c6 = new DTOCriterio();
            c6.setAtributo("cliente");
            c6.setOperador("=");
            c6.setValor(cliente);
            DTOCriterio c8 = new DTOCriterio();
            c8.setAtributo("fechaInhabilitacionCuenta");
            c8.setOperador("is null");
            c8.setValor(null);
            List<DTOCriterio> l5 = new ArrayList<>();
            l5.add(c6);
            l5.add(c8);
            List<Object> list_cuenta = FachadaPersistencia.getInstancia().buscar("Cuenta", l5);
            
            if(list_cuenta.size()>0){
                
            AdaptadorBanco adaptador_banco = FabricaAdaptadorBanco.getInstancia().obtenerAdaptadorBanco();

            List<DTOCuenta> list_dto_cuenta = new ArrayList<>();

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            //SE CREA DTOCUENTA Y SE SETEA LOS ATRIBUTOS
            for (int i = 0; i < list_cuenta.size(); i++) {

                Cuenta cuenta = (Cuenta) list_cuenta.get(i);
                DTOSaldo dtosaldo = adaptador_banco.obtenerSaldoCuenta(cuenta.getNumeroCuenta());
                DTOCuenta dto_cuenta = new DTOCuenta();
                dto_cuenta.setNombreTipoCuenta(cuenta.getTipoCuenta().getNombreTipoCuenta());
                dto_cuenta.setNumeroCuenta(cuenta.getNumeroCuenta());
                dto_cuenta.setSaldo(dtosaldo.getSaldo());

                list_dto_cuenta.add(dto_cuenta);
            }
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //SE HACE EL SETEO PARA LA INSTANCIA CONTROLSALDO, ESTA INSTANCIA SIRVE PARA UN FUTURO CONTROL.
            double saldo;
            ControlSaldo control;
            for (int i = 0; i < list_cuenta.size(); i++) {
                
                control = new ControlSaldo();
                saldo = list_dto_cuenta.get(i).getSaldo();
                control.setSaldo(saldo);
                control.setNumeroCuenta(list_dto_cuenta.get(i).getNumeroCuenta());
                list_control_saldo.add(control);

            }
            
            

            return list_dto_cuenta;
                
  
            }else{
                
                return null;
            }
            


        


        }

    

    public DTOMostrarInterfaz seleccionarCuenta(int numeroCuenta) {

        DTOCriterio c7 = new DTOCriterio();
        c7.setAtributo("numeroCuenta");
        c7.setOperador("=");
        c7.setValor(numeroCuenta);
        List<DTOCriterio> l6 = new ArrayList<>();
        l6.add(c7);

        Cuenta cuenta = (Cuenta) FachadaPersistencia.getInstancia().buscar("Cuenta", l6).get(0);

        operacion.setCuenta(cuenta);

        double minimo_monto_exigible = 0.0;
        List<OperacionAtributo> list_operacion_atributo = operacion.getListOperacionAtributo();
        for (int i = 0; i < list_operacion_atributo.size(); i++) {
            String nombreAtributo = list_operacion_atributo.get(i).getTipoImpuestoAtributo().getAtributo().getNombreAtributo();
            if ("Monto minimo exigible".equals(nombreAtributo)) {
                String nombreTipoDato = list_operacion_atributo.get(i).getTipoImpuestoAtributo().getAtributo().getTipoDato().getNombreTipoDato();
                if ("double".equals(nombreTipoDato)) {
                    minimo_monto_exigible = Double.parseDouble(list_operacion_atributo.get(i).getValorOperacionAtibuto());

                }
            }
        }

        TipoImpuesto tipoimpuesto = operacion.getTipoImpuesto();
        EmpresaTipoImpuesto empresa_tipo_impuesto = operacion.getEmpresaTipoImpuesto();

        boolean modificable = tipoimpuesto.isModificableTipoImpuesto();

        if (modificable == false) {
            for (int i = 0; i < list_control_saldo.size(); i++) {
                int numeroCuentaControl = list_control_saldo.get(i).getNumeroCuenta();
                if (numeroCuentaControl == numeroCuenta) {//Se busca el saldo de la cuenta seleccionada
                    saldoControl = list_control_saldo.get(i).getSaldo();
                }
            }
            if (operacion.getImporteOperacion() <= saldoControl) {
                DTOMostrarInterfaz mostrar_interfaz = new DTOMostrarInterfaz();
                mostrar_interfaz.setImporteOperacion(operacion.getImporteOperacion());
                mostrar_interfaz.setNombreEmpresa(empresa_tipo_impuesto.getEmpresa().getNombreEmpresa());
                mostrar_interfaz.setNombreTipoCuenta(cuenta.getTipoCuenta().getNombreTipoCuenta());
                mostrar_interfaz.setNumeroComprobante(operacion.getNumeroComprobante());
                mostrar_interfaz.setNumeroCuenta(numeroCuenta);
                mostrar_interfaz.setNombreTipoImpuesto(tipoimpuesto.getNombreTipoImpuesto());
                mostrar_interfaz.setModificable(modificable);
                return mostrar_interfaz;
            } else {

                return null;
            }

        } else {

            for (int i = 0; i < list_control_saldo.size(); i++) {
                int numeroCuentaControl = list_control_saldo.get(i).getNumeroCuenta();
                if (numeroCuentaControl == numeroCuenta) {//Se busca el saldo de la cuenta seleccionada
                    saldoControl = list_control_saldo.get(i).getSaldo();
                }
            }
            if (operacion.getImporteOperacion() <= saldoControl) {//SE CONTROLA QUE TENGA SALDO SUFICIENTE PARA ESTA OPERACION
                DTOMostrarInterfaz mostrar_interfaz = new DTOMostrarInterfaz();
                mostrar_interfaz.setImporteOperacion(operacion.getImporteOperacion());
                mostrar_interfaz.setNombreEmpresa(empresa_tipo_impuesto.getEmpresa().getNombreEmpresa());
                mostrar_interfaz.setNombreTipoCuenta(cuenta.getTipoCuenta().getNombreTipoCuenta());
                mostrar_interfaz.setNumeroComprobante(operacion.getNumeroComprobante());
                mostrar_interfaz.setNumeroCuenta(numeroCuenta);
                mostrar_interfaz.setSaldoCuenta(saldoControl);
                mostrar_interfaz.setNombreTipoImpuesto(tipoimpuesto.getNombreTipoImpuesto());
                mostrar_interfaz.setModificable(modificable);
                mostrar_interfaz.setMinimoImporteExigible(minimo_monto_exigible);
                return mostrar_interfaz;

            } else {
                return null;
            }

        }

    }
    boolean exitoEditable;

    public boolean editarImporte(int numeroCuenta, double importeEditado) {

        for (int i = 0; i < list_control_saldo.size(); i++) {
            int numeroCuentaControl = list_control_saldo.get(i).getNumeroCuenta();
            if (numeroCuentaControl == numeroCuenta) {//Se busca el saldo de la cuenta seleccionada
                saldoControl = list_control_saldo.get(i).getSaldo();
            }

        }

        if (importeEditado <= saldoControl) {
            double minimo_monto_exigible = 0.0;
            List<OperacionAtributo> list_operacion_atributo = operacion.getListOperacionAtributo();
            for (int i = 0; i < list_operacion_atributo.size(); i++) {
                String nombreAtributo = list_operacion_atributo.get(i).getTipoImpuestoAtributo().getAtributo().getNombreAtributo();
                if ("Monto minimo exigible".equals(nombreAtributo)) {
                    String nombreTipoDato = list_operacion_atributo.get(i).getTipoImpuestoAtributo().getAtributo().getTipoDato().getNombreTipoDato();
                    if ("double".equals(nombreTipoDato)) {
                        minimo_monto_exigible = Double.parseDouble(list_operacion_atributo.get(i).getValorOperacionAtibuto());

                    }
                    ///////////////////////////CONTROLAMOS QUE EL IMPORTE INGRESADO SEA MAYOR wwA MINIMO MONTO EXIGIBLE Y MENOR AL IMPORTE DE OPERACION
                    if (minimo_monto_exigible <= importeEditado) {
                        if (importeEditado <= operacion.getImporteOperacion()) {
                            operacion.setImporteOperacion(importeEditado);
                            exitoEditable = true;
                        }

                    } else {
                        exitoEditable = false;
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////

                }
            }

        } else {
            exitoEditable = false;
        }

        return exitoEditable;
    }

    public DTODebitoEstado debitarPago() {
        
        Date hoy = new Date();
        operacion.setFechaOperacion(hoy);
        Cuenta cuenta = operacion.getCuenta();
        
        AdaptadorBanco adaptador_banco = FabricaAdaptadorBanco.getInstancia().obtenerAdaptadorBanco();
        DTODebitoEstado dto_debito_estado = null;
        if (adaptador_banco instanceof AdaptadorBanco1) {
            AdaptadorBanco1 adaptador_banco1 = (AdaptadorBanco1) adaptador_banco;    
            dto_debito_estado = adaptador_banco1.generarDebito(operacion.getImporteOperacion(),cuenta.getNumeroCuenta());
            
        }
                
        return dto_debito_estado;
    }
    
    boolean informeEstado;
    public boolean informarEmpresa(){
        
        List<Object> list_operacion = FachadaPersistencia.getInstancia().buscar("Operacion", null);
        //Tenemos que buscar la operacion con el ultimo numero de operacion
        if(list_operacion.size() > 0){
        Operacion ultimaoperacion = (Operacion) list_operacion.get((list_operacion.size()-1));
        int ultimoNumeroOperacion = ultimaoperacion.getNumeroOperacion();
       int numeroOperacion = ultimoNumeroOperacion + 1;
       operacion.setNumeroOperacion(numeroOperacion);
        }else{//SI ES LA PRIMERA OPERACION ENTONCES COMIENZA CON LA ENUMERACION
            operacion.setNumeroOperacion(1);
        }
       
       
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       // INFORMAREMOS A LA EMPRESA DEL DEBITO.
       
       Empresa empresa = operacion.getEmpresaTipoImpuesto().getEmpresa();
       AdaptadorEmpresa adaptador_empresa = FabricaAdaptadorEmpresa.getInstancia().obtenerAdaptadorEmpresa(empresa);

    
            informeEstado = adaptador_empresa.informarPago(operacion.getNumeroComprobante(), operacion.getImporteOperacion());
            
        
        
        if (informeEstado==true){
            
            FachadaPersistencia.getInstancia().guardar(operacion);
            
        }
       
       return informeEstado; 
    }

}
