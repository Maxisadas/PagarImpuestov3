/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControlPagarImpuesto;

import DTO.DTOComprobanteImpago;
import DTO.DTOCuenta;
import DTO.DTODebitoEstado;
import DTO.DTOEmpresa;
import DTO.DTOMostrarInterfaz;
import DTO.DTOTipoImpuesto;
import Entidades.Usuario;
import Persistencia.FachadaInterna;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public class DecoradorPagarImpuesto extends ExpertoPagarImpuesto {

    public void iniciarTransaccion() {
        FachadaInterna.getInstancia().iniciarTransaccion();
    }

    public List<DTOTipoImpuesto> pagarImpuesto(String nombreUsuario) {

        return super.pagarImpuesto(nombreUsuario);
    }

    public List<DTOEmpresa> seleccionarTipoImpuesto(int codigoTipoImpuesto) {

        return super.seleccionarTipoImpuesto(codigoTipoImpuesto);
    }

    public void seleccionarEmpresa(int codigoEmpresaTipoImpuesto) {

        super.seleccionarEmpresa(codigoEmpresaTipoImpuesto);

    }

    public List<DTOComprobanteImpago> ingresarCodPagoElectronico(String codigoPagoElectronico) {

        return super.ingresarCodPagoElectronico(codigoPagoElectronico);
    }

    public List<DTOCuenta> seleccionarNumeroCuenta(int numeroComprobante) {

        return super.seleccionarNumeroCuenta(numeroComprobante);
    }

    public DTOMostrarInterfaz seleccionarCuenta(int numeroCuenta) {

        return super.seleccionarCuenta(numeroCuenta);
    }

    public boolean editarImporte(int numeroCuenta, double importeEditado) {

        return super.editarImporte(numeroCuenta, importeEditado);
    }

    public DTODebitoEstado debitarPago() {
        
        return super.debitarPago();
        
    }
    
    public boolean informarEmpresa(){
        
        return super.informarEmpresa();
        
    }
    
    public void finalizarTransaccion(){
        FachadaInterna.getInstancia().finalizarTransaccion();
        
    }
}
