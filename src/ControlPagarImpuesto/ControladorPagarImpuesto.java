/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControlPagarImpuesto;

import Fabricas.FabricaExpertos;
import DTO.DTOCuenta;
import DTO.DTOComprobanteImpago;
import DTO.DTODebitoEstado;
import DTO.DTOEmpresa;
import DTO.DTOMostrarInterfaz;
import DTO.DTOTipoImpuesto;
import Entidades.Usuario;
import java.util.List;

/**
 *
 * @author phantom2024
 */
public class ControladorPagarImpuesto {

    private DecoradorPagarImpuesto decorador_pagar_impuesto;

    public ControladorPagarImpuesto() {
        FabricaExpertos fabrica_expertos = FabricaExpertos.getInstancia();
        decorador_pagar_impuesto = (DecoradorPagarImpuesto) fabrica_expertos.crearExperto("PagarImpuesto");
        decorador_pagar_impuesto.iniciarTransaccion();
    }

    public List<DTOTipoImpuesto> pagarImpuesto(String nombreUsuario) {

        List<DTOTipoImpuesto> lista_tipo_impuesto = decorador_pagar_impuesto.pagarImpuesto(nombreUsuario);
        return lista_tipo_impuesto;
    }

    public List<DTOEmpresa> seleccionarTipoImpuesto(int codigoTipoImpuesto) {

        return decorador_pagar_impuesto.seleccionarTipoImpuesto(codigoTipoImpuesto);
    }

    public void seleccionarEmpresa(int codigoEmpresaTipoImpuesto) {

        decorador_pagar_impuesto.seleccionarEmpresa(codigoEmpresaTipoImpuesto);

    }

    public List<DTOComprobanteImpago> ingresarCodPagoElectronico(String codigoPagoElectronico) {

        return decorador_pagar_impuesto.ingresarCodPagoElectronico(codigoPagoElectronico);
    }

    public List<DTOCuenta> seleccionarNumeroCuenta(int numeroComprobante) {

        return decorador_pagar_impuesto.seleccionarNumeroCuenta(numeroComprobante);
    }

    public DTOMostrarInterfaz seleccionarCuenta(int numeroCuenta) {

        return decorador_pagar_impuesto.seleccionarCuenta(numeroCuenta);
    }

    public boolean editarImporte(int numeroCuenta, double importeEditado) {

        return decorador_pagar_impuesto.editarImporte(numeroCuenta, importeEditado);
    }

    public DTODebitoEstado debitarPago(){
        return decorador_pagar_impuesto.debitarPago();
    }
    
    public boolean informarEmpresa(){
        
        
        return decorador_pagar_impuesto.informarEmpresa();
        
    }
    
    public void finalizarPrograma(){
        
        decorador_pagar_impuesto.finalizarTransaccion();
    }
    
   
}
